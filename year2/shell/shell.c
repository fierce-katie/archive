#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#define INPUT 1
#define OUTPUT 2
#define AMP 0
#define ERR 1
#define QUOTE 2
#define END 3
#define APPEND 4
#define IN 5
#define OUT 6
#define ENDLN 7
#define CONV 8

struct word_list {
    char *data;
    struct word_list *next;
    int redir;
};

struct char_list {
    char data;
    struct char_list *next;
};

struct cmd_list {
    char **data;
    struct cmd_list *next;
};

int nword=0, length=0, ncmd=0;
int flags[9];
char *inputfile, *outputfile;

struct char_list *freeclist(struct char_list *cl)
{
    struct char_list *q;
    while (cl!=NULL) {
        q=cl->next;
        free(cl);
        cl=q;
    }
    return NULL;
}

struct word_list *freewlist(struct word_list *wl)
{
    struct word_list *q;
    while (wl!=NULL) {
        q=wl->next;
        free(wl);
        wl=q;
    }
    return NULL;
}


struct cmd_list *freeconv(struct cmd_list *conv)
{
    int i;
    struct cmd_list *tmp;
    while (conv!=NULL) {
        tmp=conv->next;
        i=0;
        while (conv->data[i]!=NULL)
            free(conv->data[i++]);
        free(conv->data);
        free(conv);
        conv=tmp;
    }
    return NULL;
}

/*Make command line out of word list*/
char **mkcmd(struct word_list *wl, int n)
{
    char **c=malloc(n*sizeof(*c));
    int i=0;
    while (wl!=NULL) {
        if (!wl->redir)
            c[i++]=wl->data;
        wl=wl->next;
    }
    c[i]=NULL;
    return c;
}

/*String comparison, return 0 if equal, 1 otherwise*/
int cmpstr(const char *s1,const char *s2)
{
    while ((*s1==*s2)&&*s1&&*s2) {
        s1++;
        s2++;
    }
    return (*s1-*s2);
}

/*Checking if any processes running */
int running(int **a, int len, int pid)
{
    int i, flag=0;
    for (i=0; i<len; i++) {
        if ((*a)[i]==pid)
            (*a)[i]=0;
        if ((*a)[i]!=0)
            flag=1;
    }
    return flag;
}

void redirout()
{
    int fd=0;
    if (flags[OUT]) {
        if (flags[APPEND])
            fd=open(outputfile, O_RDWR|O_CREAT|O_APPEND, 0666);
        else
            fd=open(outputfile, O_RDWR|O_CREAT|O_TRUNC, 0666);
        dup2(fd, 1);
        close(fd);
    }
    if (fd==-1) {
        perror(outputfile);
        exit(1);
    }
}

void redirin()
{
    int fd=0;
    if (flags[IN]) {
        fd=open(inputfile, O_RDONLY);
        dup2(fd, 0);
        close(fd);
    }
    if (fd==-1) {
        perror(inputfile);
        exit(1);
    }
}

void changedir(char** cmd)
{
    if (cmd[1]==NULL||cmd[2]!=NULL)
        printf("Error! Invalid arfuments for cd.\n");
    else {
        if (chdir(cmd[1])==-1)
            perror("cd");
    }
}

/*System call */
void exe(struct cmd_list *conv)
{
    int x, pipes[2], tmpfd, *pidv, i;
    char **cmd;
    pidv=malloc(ncmd*sizeof(int));
    for (i=0; i<ncmd; i++)
        pidv[i]=0;
    if (conv!=NULL)
        cmd=conv->data;
    else
        return;
    if (ncmd==1&&(cmpstr(cmd[0], "cd")==0)) {
        changedir(cmd);
    } else {
        for (i=0; i<ncmd; i++) {
            cmd=conv->data;
            conv=conv->next;
            if (i!=ncmd-1)
                pipe(pipes);
            x=fork();
            if (x==0) {
                close(pipes[0]);
                if (i!=0) {
                    dup2(tmpfd, 0);
                    close(tmpfd);
                } else {
                    redirin();
                }
                if (i!=ncmd-1) {
                    dup2(pipes[1], 1);
                    close(pipes[1]);
                } else {
                    redirout();
                }
                if (cmpstr(cmd[0], "cd")==0) {
                    exit(0);
                } else {
                    execvp(*cmd, cmd);
                    perror(*cmd);
                    exit(1);
                }
            }
            pidv[i]=x;
            if (i)
                close(tmpfd);
            if (i!=ncmd-1) {
                close(pipes[1]);
                tmpfd=dup(pipes[0]);
                close(pipes[0]);
            }
        }
    }
    if (flags[AMP])
        while (wait4(-1, NULL, WNOHANG, NULL)>0) {}
    else
        do {
            x=wait(NULL);
        } while (running((int**) &pidv, ncmd, x));
    inputfile=outputfile=NULL;
    free(pidv);
}

/*Add new word to word list*/
struct word_list *mkwlist(struct word_list *wl, struct char_list *cl, int rd)
{
    char *s;
    int i=0;
    struct word_list *tmp;
    if (cl!=NULL) {
        if (!flags[IN]&&!flags[OUT])
            nword++;
        s=malloc(length+1);
        while (cl!=NULL) {
            s[i++]=cl->data;
            cl=cl->next;
        }
        s[length]='\0';
        if (wl==NULL) {
            wl=malloc(sizeof(*wl));
            wl->data=s;
            wl->next=NULL;
            wl->redir=rd;
        } else {
            tmp=wl;
            while(tmp->next!=NULL)
                tmp=tmp->next;
            tmp->next=malloc(sizeof(*tmp));
            tmp=tmp->next;
            tmp->data=s;
            tmp->next=NULL;
            tmp->redir=rd;
        }
    }
    length=0;
    return wl;
}

/*Add new char to char list*/
struct char_list *addchar(struct char_list *cl, int c)
{
    struct char_list *q;
    ++length;
    if (cl==NULL) {
        cl=malloc(sizeof(*cl));
        cl->data=c;
        cl->next=NULL;
    } else {
        q=cl;
        while (q->next!=NULL)
            q=q->next;
        q->next=malloc(sizeof(*q));
        q=q->next;
        q->data=c;
        q->next=NULL;
    }
    return cl;
}

/*Character analysis*/
struct char_list *mkclist(struct char_list *cl, int c)
{
    if (flags[AMP])
        flags[ERR]=1;
    else
        switch (c) {
            case '&':
                if (flags[QUOTE])
                    cl=addchar(cl,c);
                else
                    flags[AMP]=1;
                break;
            case '\"':
                flags[QUOTE]=flags[QUOTE]?0:1;
                break;
            case ' ':
                if ((flags[IN]||flags[OUT])&&(cl!=NULL))
                    flags[ERR]=1;
                else
                    if (flags[QUOTE])
                        cl=addchar(cl,c);
                break;
            case '>':
                if (flags[QUOTE])
                    cl=addchar(cl,c);
                else {
                    if (flags[OUT])
                        flags[ERR]=1;
                }
                break;
            case '<':
                if (flags[QUOTE])
                    cl=addchar(cl,c);
                else {
                    if (flags[IN])
                        flags[ERR]=1;
                }
                break;
            case '|':
                if (flags[QUOTE])
                    cl=addchar(cl,c);
                else {
                    if (flags[IN]||flags[OUT])
                        flags[ERR]=1;
                    else
                        flags[CONV]=1;
                }
                break;
            default:
                cl=addchar(cl,c);
        }
    return cl;
}

/*Find input or output file name in word list*/
char *getfile(struct word_list *wl, int rd)
{
    char *s=NULL;
    while (wl!=NULL) {
        if (wl->redir==rd) {
            s=wl->data;
        }
        wl=wl->next;
    }
    return s;
}

struct word_list *readin(struct word_list *wl);

/* > */
struct word_list *readout(struct word_list *wl)
{
    struct char_list *cl=NULL;
    int c;
    flags[OUT]=1;
    c=getchar();
    if (c=='>') {
        flags[APPEND]=1;
        c=getchar();
    }
    do {
        if (c==EOF)
            flags[END]=1;
        else
            if (c!='\n')
                cl=mkclist(cl,c);
            else
                break;
        c=getchar();
    } while (!flags[END]&&(c!='\n')&&(c!='<'));
    wl=mkwlist(wl,cl,OUTPUT);
    if (c=='<') {
        if (flags[IN])
            flags[ERR]=1;
        else
            wl=readin(wl);
    }
    cl=freeclist(cl);
    return wl;
}

/* < */
struct word_list *readin(struct word_list *wl)
{
    struct char_list *cl=NULL;
    int c;
    flags[IN]=1;
    do {
        c=getchar();
        if (c==EOF)
            flags[END]=1;
        else
            if ((c!='\n')&&(c!='>'))
                cl=mkclist(cl,c);
    } while (!flags[END]&&(c!='\n')&&(c!='>'));
    wl=mkwlist(wl,cl,INPUT);
    if (c=='>') {
        if (flags[OUT])
            flags[ERR]=1;
        else
            wl=readout(wl);
    }
    cl=freeclist(cl);
    return wl;
}

/*Read single command */
struct word_list *readcmd(struct word_list *wlist)
{
    struct char_list *clist=NULL;
    int c;
    do {
        c=getchar();
        switch (c) {
            case ' ':
                if (flags[QUOTE])
                    clist=mkclist(clist, c);
                else {
                    wlist=mkwlist(wlist, clist, 0);
                    clist=freeclist(clist);
                }
                break;
            case '\n':
                flags[ENDLN]=1;
                if (flags[QUOTE])
                    flags[ERR]=1;
                else
                    wlist=mkwlist(wlist, clist, 0);
                clist=freeclist(clist);
                break;
            case EOF:
                flags[END]=1;
                break;
            case '>':
                if (flags[AMP])
                    flags[ERR]=1;
                else {
                    if (flags[QUOTE])
                        clist=addchar(clist,c);
                    else {
                        wlist=mkwlist(wlist, clist, 0);
                        wlist=readout(wlist);
                        flags[ENDLN]=1;
                    }
                }
                break;
            case '<':
                if (flags[AMP])
                    flags[ERR]=1;
                else {
                    if (flags[QUOTE])
                        clist=addchar(clist,c);
                    else {
                        wlist=mkwlist(wlist, clist, 0);
                        wlist=readin(wlist);
                        flags[ENDLN]=1;
                    }
                }
                break;
            default:
                clist=mkclist(clist, c);
        }
    } while ((!flags[END])&&(!flags[ENDLN])&&(!flags[CONV]));
    if (!flags[IN]&&!flags[OUT])
        wlist=mkwlist(wlist, clist, 0);
    clist=freeclist(clist);
    return wlist;
}

/* Add new command to commands list */
struct cmd_list *addcmd(struct cmd_list *conv, char **cmd)
{
    struct cmd_list *tmp;
    if (conv==NULL) {
        conv=malloc(sizeof(*conv));
        conv->data=cmd;
        conv->next=NULL;
    } else {
        tmp=conv;
        while (tmp->next!=NULL)
            tmp=tmp->next;
        tmp->next=malloc(sizeof(*tmp));
        tmp=tmp->next;
        tmp->data=cmd;
        tmp->next=NULL;
    }
    return conv;
}

/* Read string, making list of commands: cmd1 | cmd2 | ... */
struct cmd_list *mkconv(struct cmd_list *conv)
{
    struct word_list *wlist=NULL, *wl;
    char **cmd=NULL;
    do {
        wlist=readcmd(wlist);
        inputfile=getfile(wlist, INPUT);
        outputfile=getfile(wlist, OUTPUT);
        if ((flags[IN]&&(inputfile==NULL))||(flags[OUT]&&(outputfile==NULL)))
            flags[ERR]=1;
        if (flags[ERR]&&!flags[END])
            printf("Error! Invalid command.\n");
        else
            if (!flags[ERR]&&nword&&!flags[END]) {
                cmd=mkcmd(wlist, nword+1);
                ncmd++;
                conv=addcmd(conv, cmd);
            }
        /*wl=wlist;
          while (wl!=NULL) {
          printf("%s.  %d\n", wl->data, wl->redir);
          wl=wl->next;
          }*/
        wlist=freewlist(wlist);
        cmd=NULL;
        nword=length=0;
        flags[QUOTE]=0;
        flags[CONV]=0;
    } while (!flags[ERR]&&!flags[END]&&!flags[ENDLN]);
    return conv;
}

int main()
{
    struct cmd_list *conv=NULL;
    int i;
    do {
        for (i=0; i<=8; i++)
            flags[i]=0;
        putchar('>');
        conv=mkconv(conv);
        exe(conv);
        conv=freeconv(conv);
        ncmd=0;
    } while (!flags[END]);
    putchar('\n');
    return 0;
}
