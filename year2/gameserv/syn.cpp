#include "lex.h"
#include "poliz.h"
#include "syn.h"

void Synt::AddVar(char *var) {
    VarTable *p=new VarTable;
    p->next=NULL;
    int len=strlen(var);
    char *n=new char[len];
    strcpy(n,var);
    p->name=n;
    if (!vt)
        vt=p;
    else {
        VarTable *tmp=vt;
        while (tmp->next)
            tmp=tmp->next;
        tmp->next=p;
    }
}

void Synt::MakeArray(int n)
{
    VarTable *tmp=vt;
    while (tmp->next)
        tmp=tmp->next;
    int len=strlen(tmp->name);
    char *oldname=new char[len];
    strcpy(oldname, tmp->name);
    delete tmp->name;
    tmp->name=new char[LEN];
    for (int i=0; i<LEN; i++)
        tmp->name[i]=0;
    sprintf(tmp->name, "%s[0]", oldname);
    for (int i=1; i<n; i++) {
        char *newname=new char[LEN];
        for (int j=0; j<LEN; j++)
            newname[j]=0;
        sprintf(newname, "%s[%d]", oldname, i);
        AddVar(newname);
    }
}

void Synt::AddLab(char *str)
{
    LabelTable *l=new LabelTable;
    l->next=NULL;
    l->p=NULL;
    int len=strlen(str);
    char *n=new char[len];
    strcpy(n,str);
    l->name=n;
    LabelTable *tmp=lt;
    while (tmp->next)
        tmp=tmp->next;
    tmp->next=l;
}

void Synt::AssignLab(PolizItem *ip)
{
    if (!lt)
        throw (Exception("empty label table"));
    LabelTable *tmp=lt;
    while (tmp->next)
        tmp=tmp->next;
    tmp->p=ip;
}

PolizItem * Synt::PutPoliz(PolizElem *p)
{
    PolizItem *tmp=new PolizItem;
    tmp->pointer=p;
    tmp->next=NULL;
    last->next=tmp;
    last=tmp;
    if (label) {
        AssignLab(last);
        label=false;
    }
    return last;
}

void Synt::Program()
{
    PolizItem *tmp=new PolizItem;
    tmp->pointer=new PolizNihil;
    tmp->next=NULL;
    first=last=tmp;
    lt=new LabelTable;
    lt->name=NULL;
    lt->next=NULL;
    lt->p=tmp;
    Next();
    if (LexEq(LEX_VAR)) {
        Next();
        Declaration();
        if (LexEq(LEX_PROGRAM)) {
            Next();
            Code();
            if (LexEq(LEX_END)) {
                Next();
                if (!LexEq(LEX_EOF))
                    throw (Exception("operator or declaration outside block",
                                cur->line, cur->s));
            } else {
                if (LexEq(LEX_EOF))
                    throw (Exception("no matching \"end\" for \"program\"", cur->line));
                else
                    throw (Exception("wrong usage or expected ; before",
                                cur->line, cur->s));
            }
        } else
            if (!LexEq(LEX_EOF))
                throw (Exception("operator outside block", cur->line));
        return;
    }
    if (LexEq(LEX_PROGRAM)) {
        Next();
        Code();
        if (LexEq(LEX_END)) {
            Next();
            if (!LexEq(LEX_EOF))
                throw (Exception("operator or declaration outside block",
                            cur->line, cur->s));
        } else {
            if (LexEq(LEX_EOF))
                throw (Exception("no matching \"end\" for \"program\"", cur->line));
            else
                throw (Exception("wrong usage or expected ; before",
                            cur->line, cur->s));
        }
        return;
    }
    if (!LexEq(LEX_EOF))
        throw (Exception("operator or declaration outside block",
                    cur->line, cur->s));
}

void Synt::Declaration()
{
    if (LexEq(LEX_VARIABLE)) {
        AddVar(cur->s);
        Next();
        ConstIdx();
        DecList();
    }
}

void Synt::DecList()
{
    if (LexEq(LEX_COMMA)) {
        Next();
        if (LexEq(LEX_VARIABLE)) {
            AddVar(cur->s);
            Next();
            ConstIdx();
            DecList();
            return;
        } else
            throw (Exception("wrong declaration", cur->line, cur->s));
    }
}

void Synt::ConstIdx()
{
    if (LexEq(LEX_IDX_OPEN)) {
        Next();
        if (LexEq(LEX_NUMBER)) {
            MakeArray(cur->val);
            Next();
            if (LexEq(LEX_IDX_CLOSE))
                Next();
            else
                throw (Exception("wrong index format", cur->line, cur->s));
        } else
            throw (Exception("wrong array declaration (index not constant)",
                        cur->line));
    }
}

void Synt::Code()
{
    Instruction();
    InstList();
}

void Synt::InstList()
{
    if (LexEq(LEX_DIVIDER)) {
        Next();
        Instruction();
        InstList();
    }
}

void Synt::Instruction()
{
    Label();
    Operator();
}

void Synt::Label()
{
    if (LexEq(LEX_LABEL)) {
        AddLab(cur->s);
        label=true;
        Next();
        if (LexEq(LEX_COLON))
            Next();
        else
            throw (Exception("expected : after label", cur->line));
    }
}

void Synt::Operator()
{
    if (LexEq(LEX_GOTO)) {
        Next();
        if (LexEq(LEX_LABEL)) {
            PutPoliz(new PolizLabel(cur->s, lt));
            Next();
        } else
            throw (Exception("not a label after goto", cur->line,
                        cur->s));
        PutPoliz(new PolizJump);
        return;
    }
    if (LexEq(LEX_IF)) {
        Next();
        if (LexEq(LEX_BRACKET_OPEN)) {
            Next();
            Expr();
        } else
            throw (Exception("missing parenthes in conditional statement",
                        cur->line));
        if (LexEq(LEX_BRACKET_CLOSE))
            Next();
        else
            throw (Exception("missing parenthes in conditional statement",
                        cur->line));
        if (LexEq(LEX_THEN)) {
            PolizItem *jump=PutPoliz(new PolizNihil);
            PutPoliz(new PolizJumpFalse);
            Next();
            Instruction();
            PolizItem *p=PutPoliz(new PolizNihil);
            delete jump->pointer;
            jump->pointer=new PolizLabel(p);
        } else
            throw (Exception("expected \"then\" after condition", cur->line));
        return;
    }
    if (LexEq(LEX_PRINT)) {
        Next();
        PrintList();
        return;
    }
    if (LexEq(LEX_GAME2)) {
        PolizElem *tmp;
        if (cur->val==lex_buy)
            tmp=new PolizBuy(client);
        else
            tmp=new PolizSell(client);
        Next();
        Args2();
        PutPoliz(tmp);
        return;
    }
    if (LexEq(LEX_TURN)) {
        Next();
        PutPoliz(new PolizTurn(client));
        return;
    }
    if (LexEq(LEX_GAME1)) {
        PolizElem *tmp;
        if (cur->val==lex_prod)
            tmp=new PolizProdCmd(client);
        else
            tmp=new PolizBuild(client);
        Next();
        Args1();
        PutPoliz(tmp);
        return;
    }
    if (LexEq(LEX_VARIABLE)) {
        PutPoliz(new PolizVarAddr(cur->s));
        Next();
        Idx();
        if (LexEq(LEX_ASSIGN)) {
            Next();
            Expr();
            PutPoliz(new PolizAssign(vt));
        } else
            throw (Exception("wrong usage of variable, expected :=", cur->line));
        return;
    }
    if (LexEq(LEX_BEGIN)) {
        Next();
        Code();
        if (LexEq(LEX_END))
            Next();
        else {
            if (LexEq(LEX_EOF))
                throw (Exception("no matching \"end\" for \"begin\"", cur->line));
            else
                throw (Exception("wrong usage", cur->line, cur->s));
        }
        return;
    }
    if (LexEq(LEX_FUNC0) || LexEq(LEX_FUNC1))
        throw (Exception("function used outside expression", cur->line, cur->s));
}

void Synt::PrintList()
{
    PElem();
    PList();
}

void Synt::PList()
{
    if (LexEq(LEX_COMMA)) {
        Next();
        PElem();
        PList();
    }
}

void Synt::PElem()
{
    if (LexEq(LEX_STRING)) {
        PutPoliz(new PolizString(cur->s));
        PutPoliz(new PolizPrint);
        Next();
    } else {
        Expr();
        PutPoliz(new PolizPrint);
    }
}

void Synt::Args1()
{
    if (LexEq(LEX_BRACKET_OPEN)) {
        Next();
        Expr();
        if (LexEq(LEX_BRACKET_CLOSE))
            Next();
        else
            throw (Exception("wrong arguments", cur->line));
    } else
        throw (Exception("no arguments (1 expected)", cur->line));
}

void Synt::Args2()
{
    if (LexEq(LEX_BRACKET_OPEN)) {
        Next();
        Expr();
        if (LexEq(LEX_COMMA)) {
            Next();
            Expr();
            if (LexEq(LEX_BRACKET_CLOSE))
                Next();
            else
                throw (Exception("wrong arguments", cur->line));
        } else
            throw (Exception("wrong arguments", cur->line));
    } else
        throw (Exception("no arguments (2 expected)", cur->line));
}

void Synt::Idx()
{
    if (LexEq(LEX_IDX_OPEN)) {
        Next();
        Expr();
        if (LexEq(LEX_IDX_CLOSE)) {
            PutPoliz(new PolizIdx(vt));
            Next();
        }
        else
            throw (Exception("wrong index format", cur->line));
    }
}

void Synt::Expr()
{
    A();
    AList();
}

void Synt::AList()
{
    if (LexEq(LEX_OP1)) {
        PolizElem *tmp;
        if (cur->val==lex_eq)
            tmp=new PolizEq;
        if (cur->val==lex_less)
            tmp=new PolizLess;
        if (cur->val==lex_greater)
            tmp=new PolizGreater;
        if (cur->val==lex_le)
            tmp=new PolizLessEqual;
        if (cur->val==lex_ge)
            tmp=new PolizGreaterEqual;
        Next();
        A();
        PutPoliz(tmp);
        AList();
    }
}

void Synt::A()
{
    B();
    BList();
}

void Synt::BList()
{
    if (LexEq(LEX_OP2)) {
        PolizElem *tmp;
        if (cur->val==lex_plus)
            tmp=new PolizPlus;
        if (cur->val==lex_minus)
            tmp=new PolizMinus;
        if (cur->val==lex_or)
            tmp=new PolizOr;
        Next();
        B();
        PutPoliz(tmp);
        BList();
    }
}

void Synt::B()
{
    C();
    CList();
}

void Synt::CList()
{
    if (LexEq(LEX_OP3)) {
        PolizElem *tmp;
        if (cur->val==lex_mul)
            tmp=new PolizMul;
        if (cur->val==lex_div)
            tmp=new PolizDiv;
        if (cur->val==lex_mod)
            tmp=new PolizMod;
        if (cur->val==lex_and)
            tmp=new PolizAnd;
        if (cur->val==lex_power)
            tmp=new PolizPower;
        Next();
        C();
        PutPoliz(tmp);
        CList();
    }
}

void Synt::C()
{
    if (LexEq(LEX_NOT) || ((cur->val==lex_plus
                    || cur->val==lex_minus)&& !LexEq(LEX_NUMBER))) {
        PolizElem *tmp;
        if (cur->val==lex_minus)
            tmp=new PolizUnaryMinus;
        if (cur->val==lex_plus)
            tmp=new PolizUnaryPlus;
        if (LexEq(LEX_NOT))
            tmp=new PolizNot;
        Next();
        C();
        PutPoliz(tmp);
        return;
    }
    if (LexEq(LEX_VARIABLE)) {
        PutPoliz(new PolizVarAddr(cur->s));
        Next();
        Idx();
        PutPoliz(new PolizValue(vt));
        return;
    }
    if (LexEq(LEX_NUMBER) || LexEq(LEX_FUNC0)) {
        if (LexEq(LEX_NUMBER))
            PutPoliz(new PolizInt(cur->val));
        else {
            if (cur->val==lex_my_id)
                PutPoliz(new PolizMyId(client));
            if (cur->val==lex_my_money)
                PutPoliz(new PolizMyMoney(client));
            if (cur->val==lex_my_stuff)
                PutPoliz(new PolizMyStuff(client));
            if (cur->val==lex_my_prod)
                PutPoliz(new PolizMyProd(client));
            if (cur->val==lex_my_factories)
                PutPoliz(new PolizMyFactories(client));
            if (cur->val==lex_players)
                PutPoliz(new PolizPlayers(client));
            if (cur->val==lex_market_stuff)
                PutPoliz(new PolizMarketStuff(client));
            if (cur->val==lex_market_max_price)
                PutPoliz(new PolizMarketMaxPrice(client));
            if (cur->val==lex_market_prod)
                PutPoliz(new PolizMarketProd(client));
            if (cur->val==lex_market_min_price)
                PutPoliz(new PolizMarketMinPrice(client));
        }
        Next();
        return;
    }
    if (LexEq(LEX_FUNC1)) {
        PolizElem *tmp;
        if (cur->val==lex_money)
            tmp=new PolizMoney(client);
        if (cur->val==lex_stuff)
            tmp=new PolizStuff(client);
        if (cur->val==lex_prodf)
            tmp=new PolizProdFunc(client);
        if (cur->val==lex_factories)
            tmp=new PolizFactory(client);
        if (cur->val==lex_result_sold)
            tmp=new PolizResultSold(client);
        if (cur->val==lex_result_prod_price)
            tmp=new PolizResultProdPrice(client);
        if (cur->val==lex_result_bought)
            tmp=new PolizResultBought(client);
        if (cur->val==lex_result_stuff_price)
            tmp=new PolizResultStuffPrice(client);
        Next();
        Args1();
        PutPoliz(tmp);
        return;
    }
    if (LexEq(LEX_BRACKET_OPEN)) {
        Next();
        Expr();
        if (LexEq(LEX_BRACKET_CLOSE))
            Next();
        else
            throw (Exception("parentheses in expression not balanced", cur->line));
        return;
    }
    throw (Exception("wrong expression format", cur->line));
}



