#include <stdio.h>
#include "lex.h"
#include "syn.h"
#include "poliz.h"
#include "client.h"

int main(int argc, char **argv)
{
    if (argc!=4) {
        printf("Wrong starting attributes\n");
        return -1;
    }
    FILE *fd=fopen(argv[3], "r");
    if (!fd) {
        perror("Unable to open file");
        return -1;
    }
    printf("Input file: %s\n", argv[3]);
    Client c(argv);
    try {
        Synt s(MakeLexemList(fd), &c);
        PolizItem *begin, *sp=NULL;;
        begin=s.Run();
        if (!c.GetStat())
            return -1;
        printf("Connected\n");
        c.StartGame();
        while (c.GetStat()) {
            if (c.me.money<=0)
                break;
            PolizItem *ip=begin;
            while (ip)
                ip->pointer->Evaluate(&ip, &sp);
            c.GetInfo(turn, c.me);
            c.me.sellres=c.m.plist[c.me.n-1].sellres;
            c.me.buyres=c.m.plist[c.me.n-1].buyres;
            c.NewInfo();
        }
    }
    catch (Exception &expt) {
        expt.Print();
        fclose(fd);
        c.End();
        return -1;
    }
    fclose(fd);
    c.End();
    printf("Game ended\n");
    return 0;
}
