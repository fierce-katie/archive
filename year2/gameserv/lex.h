#ifndef LEX_INCLUDED
#define LEX_INCLUDED

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#define LEN 128

enum lex_type {
    LEX_VAR,
    LEX_PROGRAM,
    LEX_BEGIN,
    LEX_END,
    LEX_VARIABLE,
    LEX_DIVIDER,
    LEX_IDX_OPEN,
    LEX_IDX_CLOSE,
    LEX_NUMBER,
    LEX_LABEL,
    LEX_COLON,
    LEX_GOTO,
    LEX_IF,
    LEX_BRACKET_OPEN,
    LEX_BRACKET_CLOSE,
    LEX_THEN,
    LEX_PRINT,
    LEX_GAME2,
    LEX_GAME1,
    LEX_TURN,
    LEX_FUNC0,
    LEX_FUNC1,
    LEX_ASSIGN,
    LEX_COMMA,
    LEX_STRING,
    LEX_OP1,
    LEX_OP2,
    LEX_OP3,
    LEX_NOT,
    LEX_EOF,
    RET,
};

enum lex_val {
    lex_plus,
    lex_minus,
    lex_mul,
    lex_div,
    lex_mod,
    lex_power,
    lex_and,
    lex_or,
    lex_not,
    lex_eq,
    lex_less,
    lex_greater,
    lex_le,  // <=
    lex_ge,  // >=
    lex_variable,  // $name
    lex_label,  // @name
    lex_goto,  // keywords
    lex_if,
    lex_then,
    lex_var,
    lex_program,
    lex_begin,
    lex_end,
    lex_print,
    lex_buy,
    lex_sell,
    lex_prod,
    lex_build,
    lex_turn,
    lex_my_id,  // functions
    lex_my_money,
    lex_my_stuff,
    lex_my_prod,
    lex_my_factories,
    lex_players,
    lex_money,
    lex_prodf,
    lex_stuff,
    lex_factories,
    lex_market_stuff,
    lex_market_min_price,
    lex_market_prod,
    lex_market_max_price,
    lex_result_sold,
    lex_result_prod_price,
    lex_result_bought,
    lex_result_stuff_price,
    lex_string,  // "str"
    lex_divider,  // ;
    lex_comma,
    lex_colon,
    lex_assign,  // :=
    lex_idx_open,  // [
    lex_idx_close,  // ]
    lex_bracket_open,  // (
    lex_bracket_close,  // )
    ret
};

class Exception {
    const char *msg;
    int line;
    char lex[LEN];
    public:
    Exception(const char *s, int n=0, char *l=NULL) {
        msg=s; line=n; if (l) strcpy(lex,l); else *lex=0;
    }
    void Print() {
        if (line)
            printf("Error in line %d:  %s", line, msg);
        else
            printf("Error: %s", msg);
        if (*lex)
            printf(": %s", lex);
        putchar('\n');
    }
};

struct Lexem {
    char s[LEN];
    int line;
    lex_type type;
    int val;
    Lexem *next;
    Lexem(char buf[LEN], int l) {
        strcpy(s,buf);
        line=l;
        if (strlen(buf)) {
            val=GetLexVal();
            type=GetLexType();
        } else
            type=LEX_EOF;
        next=NULL;
    }
    private:
    lex_type GetLexType();
    int GetLexVal();
};

class LexList {
    Lexem *first;
    Lexem *run;
    public:
    LexList() { first=NULL; }
    void AddLexem(char buf[LEN], int line);
    Lexem * NextLex();
    void Print();
};

class Automat {
    enum state { H, N, I, K, S, D };
    state ST;
    int line;
    char buf[LEN];
    LexList list;
    void H_State(int c);
    void N_State(int c);
    void I_State(int c);
    void K_State(int c);
    void S_State(int c);
    void D_State(int c);
    bool Number(int c);
    bool Letter(int c);
    bool Divider(int c);
    void NewLexem();
    void Step(int c);
    void Add(int c) {
        if (strlen(buf)<LEN)
            buf[strlen(buf)]=c;
        else
            throw (Exception("max length exceeded", line));
    }
    public:
    Automat() {
        ST=H; line=1;
        for (int i=0; i<LEN; i++)
            buf[i]=0;
    }
    LexList GetList() { return list; }
    void FeedChar(int c) {
        Step(c);
        if (c=='\n')
            line++;
    }
};

LexList MakeLexemList(FILE * fd);

#endif
