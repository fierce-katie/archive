#include <stdio.h>
#include "lex.h"
#include "syn.h"
#include "poliz.h"

int main(int argc, char **argv)
{
    if (argc!=2) {
        printf("Wrong starting attributes\n");
        return -1;
    }
    FILE *fd=fopen(argv[1], "r");
    if (!fd) {
        perror("Unable to open file");
        return -1;
    }
    printf("Input file: %s\n", argv[1]);
    try {
        LexList lex=MakeLexemList(fd);
        Synt s(lex);
        PolizItem *ip, *sp=NULL;
        ip=s.Run();
        while (ip)
            ip->pointer->Evaluate(&ip, &sp);
        printf("Ok!\n");
        fclose(fd);
    }
    catch (Exception &expt) {
        expt.Print();
        fclose(fd);
        return -1;
    }
    return 0;
}
