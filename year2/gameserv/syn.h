#ifndef SYN_INDLUDED
#define SYN_INCLUDED

struct PolizItem;
struct VarTable;
struct LabelTable;
class PolizElem;
class Client;

class Synt {
    Client *client;
    PolizItem *first, *last;
    PolizItem *PutPoliz(PolizElem *p);
    VarTable *vt;
    void AddVar(char *var);
    void MakeArray(int n);
    bool label;
    LabelTable *lt;
    void AddLab(char *str);
    void AssignLab(PolizItem *ip);
    LexList lex;
    Lexem *cur;
    void Program();
    void Declaration();
    void DecList();
    void ConstIdx();
    void Code();
    void InstList();
    void Instruction();
    void Label();
    void Operator();
    void PrintList();
    void PList();
    void PElem();
    void Args1();
    void Args2();
    void Idx();
    void Expr();
    void AList();
    void A();
    void BList();
    void B();
    void CList();
    void C();
    void Next() { cur= lex.NextLex(); }
    bool LexEq(lex_type type) { return (cur->type==type); }
    public:
    Synt(LexList list, Client *c)
    {
        lex=list;
        label=false;
        vt=NULL;
        lt=NULL;
        client=c;
    }
    PolizItem * Run() { Program(); return first; }
};

#endif
