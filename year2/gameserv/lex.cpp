#include "lex.h"

const char *lexem_types[]= {
    "LEX_VAR",
    "LEX_PROGRAM",
    "LEX_BEGIN",
    "LEX_END",
    "LEX_VARIABLE",
    "LEX_DIVIDER",
    "LEX_IDX_OPEN",
    "LEX_IDX_CLOSE",
    "LEX_NUMBER",
    "LEX_LABEL",
    "LEX_COLON",
    "LEX_GOTO",
    "LEX_IF",
    "LEX_BRACKET_OPEN",
    "LEX_BRACKET_CLOSE",
    "LEX_THEN",
    "LEX_PRINT",
    "LEX_GAME2",
    "LEX_GAME1",
    "LEX_TURN",
    "LEX_FUNC0",
    "LEX_FUNC1",
    "LEX_ASSIGN",
    "LEX_COMMA",
    "LEX_STRING",
    "LEX_OP1",
    "LEX_OP2",
    "LEX_OP3",
    "LEX_NOT",
    "LEX_EOF",
    "RET",
};

// LexList methods
void LexList::AddLexem(char buf[LEN], int line)
{
    Lexem *tmp=new Lexem(buf, line);
    if (!first) {
        first=run=tmp;
    }
    else {
        Lexem *p=first;
        while (p->next!=NULL)
            p=p->next;
        p->next=tmp;
    }
}

Lexem * LexList::NextLex()
{
    Lexem *tmp=run;
    if (run)
        run=run->next;
    return tmp;
}

void LexList::Print()
{
    Lexem *tmp=first;
    while (tmp) {
        printf("< %s > - line %d - type %s\n", tmp->s, tmp->line,
                lexem_types[tmp->type]);
        tmp=tmp->next;
    }
}

// Lexem methods
lex_type Lexem::GetLexType()
{
    if ((*s>='0')&&(*s<='9'))
        return LEX_NUMBER;
    if (val==lex_var)
        return LEX_VAR;
    if (val==lex_program)
        return LEX_PROGRAM;
    if (val==lex_begin)
        return LEX_BEGIN;
    if (val==lex_end)
        return LEX_END;
    if (val==lex_variable)
        return LEX_VARIABLE;
    if (val==lex_divider)
        return LEX_DIVIDER;
    if (val==lex_idx_open)
        return LEX_IDX_OPEN;
    if (val==lex_idx_close)
        return LEX_IDX_CLOSE;
    if (val==lex_label)
        return LEX_LABEL;
    if (val==lex_colon)
        return LEX_COLON;
    if (val==lex_goto)
        return LEX_GOTO;
    if (val==lex_if)
        return LEX_IF;
    if (val==lex_bracket_open)
        return LEX_BRACKET_OPEN;
    if (val==lex_bracket_close)
        return LEX_BRACKET_CLOSE;
    if (val==lex_then)
        return LEX_THEN;
    if (val==lex_print)
        return LEX_PRINT;
    if (val==lex_buy || val==lex_sell)
        return LEX_GAME2;
    if (val==lex_prod || val==lex_build)
        return LEX_GAME1;
    if (val==lex_turn)
        return LEX_TURN;
    if (val==lex_assign)
        return LEX_ASSIGN;
    if (val==lex_comma)
        return LEX_COMMA;
    if (val==lex_string)
        return LEX_STRING;
    if (val==lex_not)
        return LEX_NOT;
    if (val==lex_eq || val==lex_less || val==lex_greater || val==lex_le
            || val==lex_ge)
        return LEX_OP1;
    if (val==lex_plus || val==lex_minus || val==lex_or)
        return LEX_OP2;
    if (val==lex_mul || val==lex_div || val==lex_mod || val==lex_and
            || val==lex_power)
        return LEX_OP3;
    if (*s=='?') {
        if (val==lex_money || val==lex_stuff || val==lex_prodf || val==lex_factories
                || val==lex_result_sold || val==lex_result_prod_price
                || val==lex_result_bought || val==lex_result_stuff_price)
            return LEX_FUNC1;
        else
            return LEX_FUNC0;
    }
    return RET;
}

int Lexem::GetLexVal()
{
    if (*s>='0'&&*s<='9')
        return atoi(s);
    if (*s>='a'&&*s<='z') {
        if (!strcmp(s,"goto"))
            return lex_goto;
        if (!strcmp(s,"if"))
            return lex_if;
        if (!strcmp(s,"then"))
            return lex_then;
        if (!strcmp(s,"var"))
            return lex_var;
        if (!strcmp(s,"program"))
            return lex_program;
        if (!strcmp(s,"begin"))
            return lex_begin;
        if (!strcmp(s,"end"))
            return lex_end;
        if (!strcmp(s,"print"))
            return lex_print;
        if (!strcmp(s,"buy"))
            return lex_buy;
        if (!strcmp(s,"sell"))
            return lex_sell;
        if (!strcmp(s,"prod"))
            return lex_prod;
        if (!strcmp(s,"build"))
            return lex_build;
        if (!strcmp(s,"turn"))
            return lex_turn;
        throw (Exception("unknown key word",line,s));
    }
    switch (*s) {
        case '+': return lex_plus;
        case '-': return lex_minus;
        case '*': return lex_mul;
        case '/': return lex_div;
        case '%': return lex_mod;
        case '^': return lex_power;
        case '&': return lex_and;
        case '|': return lex_or;
        case '!': return lex_not;
        case '=': return lex_eq;
        case '"': return lex_string;
        case ';': return lex_divider;
        case ',': return lex_comma;
        case ':':
                  if (s[1]=='=')
                      return lex_assign;
                  else
                      return lex_colon;
        case '[': return lex_idx_open;
        case ']': return lex_idx_close;
        case '(': return lex_bracket_open;
        case ')': return lex_bracket_close;
        case '<':
                  if (s[1]=='=')
                      return lex_le;
                  else
                      return lex_less;
        case '>':
                  if (s[1]=='=')
                      return lex_ge;
                  else
                      return lex_greater;
        case '$': return lex_variable;
        case '@': return lex_label;
        case '?':
                  if (!strcmp(s,"?my_id"))
                      return lex_my_id;
                  if (!strcmp(s,"?my_money"))
                      return lex_my_money;
                  if (!strcmp(s,"?my_stuff"))
                      return lex_my_stuff;
                  if (!strcmp(s,"?my_prod"))
                      return lex_my_prod;
                  if (!strcmp(s,"?my_factories"))
                      return lex_my_factories;
                  if (!strcmp(s,"?players"))
                      return lex_players;
                  if (!strcmp(s,"?money"))
                      return lex_money;
                  if (!strcmp(s,"?prod"))
                      return lex_prodf;
                  if (!strcmp(s,"?stuff"))
                      return lex_stuff;
                  if (!strcmp(s,"?factories"))
                      return lex_factories;
                  if (!strcmp(s,"?market_stuff"))
                      return lex_market_stuff;
                  if (!strcmp(s,"?market_min_price"))
                      return lex_market_min_price;
                  if (!strcmp(s,"?market_prod"))
                      return lex_market_prod;
                  if (!strcmp(s,"?market_max_price"))
                      return lex_market_max_price;
                  if (!strcmp(s,"?lex_result_sold"))
                      return lex_result_sold;
                  if (!strcmp(s,"?result_prod_price"))
                      return lex_result_prod_price;
                  if (!strcmp(s,"?result_bought"))
                      return lex_result_bought;
                  if (!strcmp(s,"?result_stuff_price"))
                      return lex_result_stuff_price;
                  throw (Exception("unknown function",line,s));
    }
    return ret;
}

// Automat methods
void Automat::H_State(int c)
{
    if (c==' ' || c=='\n' || c=='\t' || c=='\r')
        return;
    if (c==EOF) {
        NewLexem();
        return;
    }
    Add(c);
    if (Number(c)) {
        ST=N;
        return;
    }
    if (c=='?' || c=='@' || c=='$') {
        ST=I;
        return;
    }
    if (Letter(c)) {
        ST=K;
        return;
    }
    if (c=='"') {
        ST=S;
        return;
    }
    if (c==':' || c=='>' || c=='<') {
        ST=D;
        return;
    }
    if (Divider(c)) {
        NewLexem();
        ST=H;
        return;
    }
    throw (Exception("wrong name format", line));
}

void Automat::N_State(int c)
{
    if (Number(c)) {
        Add(c);
        return;
    }
    if (Divider(c)) {
        NewLexem();
        ST=H;
        if ( !(c==' ' || c=='\n' || c=='\t' || c=='\r') )
            Step(c);
        return;
    }
    throw (Exception("wrong number format", line));
}

void Automat::I_State(int c)
{
    if (Letter(c) || Number(c) || c=='_') {
        Add(c);
        return;
    }
    if (Divider(c)) {
        NewLexem();
        ST=H;
        if ( !(c==' ' || c=='\n' || c=='\t' || c=='\r') )
            Step(c);
        return;
    }
    throw (Exception("wrong identifier format", line));
}

void Automat::K_State(int c)
{
    if (Letter(c)) {
        Add(c);
        return;
    }
    if (Divider(c)) {
        NewLexem();
        ST=H;
        if ( !(c==' ' || c=='\n' || c=='\t' || c=='\r') )
            Step(c);
        return;
    }
    throw (Exception("wrong keyword format", line));
}

void Automat::S_State(int c)
{
    if (c==EOF)
        throw (Exception("unexpected end of string", line));
    Add(c);
    if (c=='"') {
        NewLexem();
        ST=H;
    }
}

void Automat::D_State(int c)
{
    if (c==EOF)
        throw (Exception("unexpected end of file", line));
    if (c=='=')
        Add(c);
    else {
        NewLexem();
        ST=H;
        Step(c);
    }
}

void Automat::NewLexem()
{
    list.AddLexem(buf, line);
    for (int i=0; i<LEN; i++)
        buf[i]=0;
}

bool Automat::Number(int c)
{
    return (c>='0')&&(c<='9');
}

bool Automat::Letter(int c)
{
    return ((c>='a')&&(c<='z')) || ((c>='A')&&(c<='Z'));
}

bool Automat::Divider(int c)
{
    return ( (c=='+' || c=='-' || c=='*' || c=='/' || c=='%' || c=='^')
            || (c=='=' || c=='!' || c=='&' || c=='|' || c=='>' || c=='<')
            || (c==',' || c==';' || c==':')
            || (c=='(' || c==')' || c=='[' || c==']')
            || (c==' ' || c=='\n' || c=='\t' || c=='\r') );
}

void Automat::Step(int c)
{
    switch (ST) {
        case H:
            H_State(c);
            break;
        case N:
            N_State(c);
            break;
        case I:
            I_State(c);
            break;
        case K:
            K_State(c);
            break;
        case S:
            S_State(c);
            break;
        case D:
            D_State(c);
    }
}

extern LexList MakeLexemList(FILE * fd)
{
    Automat a;
    int c;
    do {
        c=fgetc(fd);
        a.FeedChar(c);
    } while (c!=EOF);
    return a.GetList();
}
