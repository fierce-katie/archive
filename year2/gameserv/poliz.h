#ifndef POLIZ_INCLUDED
#define POLIZ_INCLUDED

int power(int x, int n);

struct VarTable {
    char *name;
    int value;
    VarTable *next;
};

void asgn(VarTable *t, char *var, int value);

int getval(VarTable *t, char *var);

class PolizElem;

struct PolizItem {
    PolizElem *pointer;
    PolizItem *next;
};

struct LabelTable {
    char *name;
    PolizItem *p;
    LabelTable *next;
};

PolizItem *getlab(LabelTable *t, char *name);

class Client;

class PolizElem {
    public:
        virtual ~PolizElem() {}
        virtual void Evaluate(PolizItem **ip, PolizItem **sp) const=0;
        virtual void Text() const=0;
    protected:
        static void Push(PolizItem **sp, PolizElem *elem);
        static PolizElem *Pop(PolizItem **sp);
};

class PolizJump: public PolizElem {
    public:
        void Evaluate(PolizItem **ip, PolizItem **sp) const;
        void Text() const;
};

class PolizJumpFalse: public PolizElem {
    public:
        void Evaluate(PolizItem **ip, PolizItem**sp) const;
        void Text() const;
};

class PolizRegular: public PolizElem {
    public:
        virtual void EvalReg(PolizItem **sp) const =0;
        void Evaluate(PolizItem **ip, PolizItem **sp) const;
};

class PolizNihil: public PolizRegular {
    public:
        void EvalReg(PolizItem **sp) const {}
        void Text() const;
};

class PolizConst: public PolizRegular {
    public:
        void EvalReg(PolizItem **sp) const;
        virtual PolizElem *Clone() const =0;
};

//Const
class PolizVarAddr: public PolizConst {
    char *name;
    public:
    PolizVarAddr(char *str);
    char *Get() const;
    PolizElem *Clone() const;
    void Text() const;
};

class PolizLabel: public PolizConst {
    PolizItem *p;
    char *name;
    LabelTable *lt;
    public:
    PolizLabel(PolizItem *ap);
    PolizLabel(char *str, LabelTable *t);
    PolizItem *Get() const;
    PolizElem *Clone() const;
    void Text() const;
};

class PolizString: public PolizConst {
    char *s;
    public:
    PolizString(char *as);
    char *Get() const;
    PolizElem *Clone() const;
    void Text() const;
};

class PolizInt: public PolizConst {
    int n;
    public:
    PolizInt(int an);
    int Get() const;
    PolizElem *Clone() const;
    void Text() const;
};

//Functions
class PolizFunc1: public PolizRegular {
    public:
        void EvalReg(PolizItem **sp) const;
        virtual void Execute(PolizItem **sp, int op) const=0;
};

class PolizFunc2: public PolizRegular {
    public:
        void EvalReg(PolizItem **sp) const;
        virtual void Execute(PolizItem **sp, int op1, int op2) const=0;
};

//Functions without arguments
class PolizTurn: public PolizRegular {
    Client *c;
    public:
    PolizTurn(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMyId: public PolizRegular {
    Client *c;
    public:
    PolizMyId(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMyMoney: public PolizRegular {
    Client *c;
    public:
    PolizMyMoney(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMyStuff: public PolizRegular {
    Client *c;
    public:
    PolizMyStuff(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMyProd: public PolizRegular {
    Client *c;
    public:
    PolizMyProd(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMyFactories: public PolizRegular {
    Client *c;
    public:
    PolizMyFactories(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizPlayers: public PolizRegular {
    Client *c;
    public:
    PolizPlayers(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMarketStuff: public PolizRegular {
    Client *c;
    public:
    PolizMarketStuff(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMarketMinPrice: public PolizRegular {
    Client *c;
    public:
    PolizMarketMinPrice(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMarketProd: public PolizRegular {
    Client *c;
    public:
    PolizMarketProd(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizMarketMaxPrice: public PolizRegular {
    Client *c;
    public:
    PolizMarketMaxPrice(Client *ac) { c=ac; }
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizPrint: public PolizRegular {
    public:
        void EvalReg(PolizItem **sp) const;
        void Text() const;
};

class PolizIdx: public PolizRegular {
    VarTable *vt;
    public:
    PolizIdx(VarTable *p);
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizValue: public PolizRegular {
    VarTable *vt;
    public:
    PolizValue(VarTable *p);
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

class PolizAssign: public PolizRegular {
    VarTable *vt;
    public:
    PolizAssign(VarTable *p);
    void EvalReg(PolizItem **sp) const;
    void Text() const;
};

//Functions with 1 argument
class PolizUnaryPlus: public PolizFunc1 {
    public:
        void Execute(PolizItem **sp, int op) const;
        void Text() const;
};

class PolizUnaryMinus: public PolizFunc1 {
    public:
        void Execute(PolizItem **sp, int op) const;
        void Text() const;
};

class PolizNot: public PolizFunc1 {
    public:
        void Execute(PolizItem **sp, int op) const;
        void Text() const;
};

class PolizProdCmd: public PolizFunc1 {
    Client *c;
    public:
    PolizProdCmd(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizBuild: public PolizFunc1 {
    Client *c;
    public:
    PolizBuild(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizMoney: public PolizFunc1 {
    Client *c;
    public:
    PolizMoney(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizStuff: public PolizFunc1 {
    Client *c;
    public:
    PolizStuff(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizProdFunc: public PolizFunc1 {
    Client *c;
    public:
    PolizProdFunc(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizFactory: public PolizFunc1 {
    Client *c;
    public:
    PolizFactory(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizResultSold: public PolizFunc1 {
    Client *c;
    public:
    PolizResultSold(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizResultProdPrice: public PolizFunc1 {
    Client *c;
    public:
    PolizResultProdPrice(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizResultBought: public PolizFunc1 {
    Client *c;
    public:
    PolizResultBought(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

class PolizResultStuffPrice: public PolizFunc1 {
    Client *c;
    public:
    PolizResultStuffPrice(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op) const;
    void Text() const;
};

//Functions with 2 arguments
class PolizPlus: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizMinus: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizMul: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizDiv: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizMod: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizPower: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizEq: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizGreater: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizGreaterEqual: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizLess: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizLessEqual: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizAnd: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizOr: public PolizFunc2 {
    public:
        void Execute(PolizItem **sp, int op1, int op2) const;
        void Text() const;
};

class PolizBuy: public PolizFunc2 {
    Client *c;
    public:
    PolizBuy(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op1, int op2) const;
    void Text() const;
};

class PolizSell: public PolizFunc2 {
    Client *c;
    public:
    PolizSell(Client *ac) { c=ac; }
    void Execute(PolizItem **sp, int op1, int op2) const;
    void Text() const;
};

#endif
