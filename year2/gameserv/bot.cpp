#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>

enum command {
    market,
    player,
    prod,
    buy,
    sell,
    build,
    turn,
    start,
};

struct auction {
    int items;
    int price;
};

struct player {
    int n;
    long int money;
    int stuff;
    int prod;
    int fact;
    struct auction buyres;
    struct auction sellres;
};

struct market {
    int npl;
    int stuff;
    int minprice;
    int prod;
    int maxprice;
    struct player *plist;
};

class Client {
    int sockfd;
    bool connected;
    bool makesocket(char * const *argv);
    class Buf {
        friend class Client;
        char buf[1024];
        int len;
        int sockfd;
        Buf() { len=0; for (int i=0; i<1024; i++) buf[i]=0; };
        virtual ~Buf() {};
        char *getstr();
        char *takestr(int len);
        char *mystrchr(const char c);
    };
    friend class Buf;
    Buf b;
    public:
    Client(char * const *argv) { connected=makesocket(argv); };
    virtual ~Client() {};
    bool getstat() { return connected; };
    void sendcmd(enum command cmd, int arg1, int arg2);
    void getinfo(enum command cmd, struct player &pl, struct market &m);
    void addres(char *info, struct player *pls);
    void end() {
        int tmp[128];
        while (read(sockfd, tmp, 128)) {}
        shutdown(sockfd,2); close(sockfd);
    };
};

bool Client::makesocket(char * const *argv)
{
    int port;
    struct sockaddr_in addr;
    if (!sscanf(argv[2], "%d", &port)) {
        printf("Wrong port number\n");
        return false;
    }
    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);
    if (!inet_aton(argv[1], &(addr.sin_addr))) {
        printf("Wrong ip\n");
        return false;
    }
    sockfd=socket(AF_INET, SOCK_STREAM, 0);
    b.sockfd=sockfd;
    if (connect(sockfd, (struct sockaddr *) &addr, sizeof(addr))) {
        printf("Connection failed\n");
        return false;
    }
    return true;
}

void Client::sendcmd(enum command cmd, int arg1=0, int arg2=0)
{
    if (connected) {
        char buf[64];
        for (int i=0; i<64; i++)
            buf[i]=0;
        switch (cmd) {
            case market:
                sprintf(buf, "market\n\r");
                break;
            case player:
                if (arg1)
                    sprintf(buf, "player %d\n\r", arg1);
                else
                    sprintf(buf, "player\n\r");
                break;
            case prod:
                sprintf(buf, "prod %d\n\r", arg1);
                break;
            case buy:
                sprintf(buf, "buy %d %d\n\r", arg1, arg2);
                break;
            case sell:
                sprintf(buf, "sell %d %d\n\r", arg1, arg2);
                break;
            case build:
                sprintf(buf, "build\n\r");
                break;
            case turn:
                sprintf(buf, "turn\n\r");
                break;
            case start:;
        }
        write(sockfd, buf, strlen(buf));
    }
}

char * Client::Buf::takestr(int l)
{
    char *tmp=(char *)malloc(l);
    for (int i=0; i<l-1; i++)
        tmp[i]=buf[i];
    tmp[l-1]=0;
    return tmp;
}

char * Client::Buf::mystrchr(const char c)
{
    char *tmp=NULL;
    if (len>0)
        for (tmp=buf; (*tmp!=c) && (tmp!=buf+len-1); tmp++) {}
    return tmp;
}

char * Client::Buf::getstr()
{
    while (true) {
        int n=0;
        if (mystrchr('\n')==NULL)
            if (!(n=read(sockfd, buf+len, 512)))
                return NULL;
        len+=n;
        char *endl;
        int l;
        while ((endl=mystrchr('\n'))!=NULL) {
            char *info=NULL;
            l=endl-buf+1;
            if (buf[0]=='>' || buf[0]=='*' || buf[0]=='%')
                info=takestr(l);
            memmove(buf, buf+l, len-l);
            len-=l;
            for (int i=len; i<1024; i++)
                buf[i]=0;
            if (info)
                return info;
        }
    }
}

void Client::addres(char *info, struct player *pls)
{
    int n, i, p;
    if (info[0]=='*') {
        sscanf(info+2, "%d %d %d", &n, &i, &p);
        pls[n-1].buyres.items=i;
        pls[n-1].buyres.price=p;
        printf("Player %d bought %d for %d\n", n, i, p);
    } else {
        sscanf(info+1, "%d %d %d", &n, &i, &p);
        pls[n-1].sellres.items=i;
        pls[n-1].sellres.price=p;
        printf("Player %d sold %d for %d\n", n, i, p);
    }
}

void Client::getinfo(enum command cmd, struct player &pl,
        struct market &m)
{
    if (!connected)
        return;
    char *info;
    info=b.getstr();
    if (!info) {
        connected=false;
        return;
    }
    switch (cmd) {
        case market:
            sscanf(info+1, "%d %d %d %d", &m.stuff, &m.minprice,
                    &m.prod, &m.maxprice);
            free(info);
            break;
        case player:
            sscanf(info+1, "%ld %d %d %d", &pl.money, &pl.stuff,
                    &pl.prod, &pl.fact);
            free(info);
            break;
        case start:
            sscanf(info+1, "%d %d", &pl.n, &m.npl);
            break;
        case turn:
            for (int i=0; i<m.npl; i++) {
                m.plist[i-1].buyres.items=m.plist[i-1].buyres.price=0;
                m.plist[i-1].sellres.items=m.plist[i-1].sellres.price=0;
            }
            while (strcmp(info, "%")) {
                addres(info, m.plist);
                if((info=b.getstr())==NULL) {
                    connected=false;
                    return;
                }
            }
            break;
        case prod: case buy: case sell: case build:;
    }
}

void play(Client &c, struct player me, struct market m)
{
    int tmp=(me.stuff<2)?me.stuff:2;
    for (int i=tmp; i>0; i--)
        if (me.money>=tmp*2000) {
            c.sendcmd(prod, tmp);
            break;
        }
    if (me.money>=2*m.minprice) {
        c.sendcmd(buy, 2, m.minprice);
        me.money-=2*m.minprice;
    } else {
        if (me.money>=m.minprice) {
            c.sendcmd(buy, 1, m.minprice);
            me.money-=m.minprice;
        }
    }
    if (me.prod)
        c.sendcmd(sell, me.prod, m.maxprice);
}

void newinfo(Client &c, struct player &me, struct market &m)
{
    c.sendcmd(player);
    c.getinfo(player, me, m);
    printf("\nPlayer: %ld %d %d %d\n\n", me.money, me.stuff, me.prod, me.fact);
    c.sendcmd(market);
    c.getinfo(market, me, m);
    printf("Market: %d %d %d %d\n\n", m.stuff, m.minprice, m.prod, m.maxprice);
    for (int i=0; i<m.npl; i++) {
        c.sendcmd(player, i+1);
        c.getinfo(player, m.plist[i], m);
        printf("Player %d: %ld %d %d %d\n\n", i+1, m.plist[i].money,
                m.plist[i].stuff,
                m.plist[i].prod, m.plist[i].fact);
    }
}

void runbot(Client &c)
{
    struct player me;
    struct market m;
    c.getinfo(start, me, m);
    printf("Me=%d, players=%d\n", me.n, m.npl);
    m.plist=(struct player *)malloc(m.npl*sizeof(me));
    for (int i=0; i<m.npl; i++)
        m.plist[i].n=i+1;
    while (c.getstat()) {
        newinfo(c, me, m);
        if (me.money<=0)
            break;
        play(c, me, m);
        c.sendcmd(turn);
        c.getinfo(turn, me, m);
        me.buyres.items=me.buyres.price=0;
        me.sellres.items=me.sellres.price=0;
        if (m.plist[me.n-1].sellres.items)
            me.sellres=m.plist[me.n-1].sellres;
        if (m.plist[me.n-1].buyres.items)
            me.buyres=m.plist[me.n-1].buyres;

    }
}

int main(int argc, char **argv)
{
    if (argc!=3) {
        printf("Wrong starting attributes\n");
        return -1;
    }
    Client c(argv);
    if (!c.getstat())
        return -1;
    printf("Connected\n");
    runbot(c);
    c.end();
    printf("Game ended\n");
    return 0;
}

