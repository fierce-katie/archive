#include "poliz.h"
#include "lex.h"
#include "client.h"
#include <stdio.h>
#include <string.h>
// TURN!!!!
int power(int x, int n)
{
    if (!n)
        return 1;
    else
        return x*power(x, n-1);
}

void asgn(VarTable *t, char *var, int value)
{
    VarTable *tmp=t;
    while (strcmp(tmp->name, var) && tmp) {
        tmp=tmp->next;
        if (!tmp)
            throw (Exception("undeclared variable",0,var));
    }
    tmp->value=value;
}

int getval(VarTable *t, char *var)
{
    VarTable *tmp=t;
    while (strcmp(tmp->name, var) && tmp) {
        tmp=tmp->next;
        if (!tmp)
            throw (Exception("undeclared variable",0,var));
    }
    return tmp->value;
}

PolizItem *getlab(LabelTable *t, char *name)
{
    LabelTable *tmp=t->next;
    while (strcmp(tmp->name, name) && tmp) {
        tmp=tmp->next;
        if (!tmp)
            throw (Exception("undeclared label",0,name));
    }
    return tmp->p;
}

void PolizElem::Push(PolizItem **sp, PolizElem *elem)
{
    PolizItem *tmp=new PolizItem;
    tmp->pointer=elem;
    tmp->next=(*sp);
    (*sp)=tmp;
}

PolizElem* PolizElem::Pop(PolizItem **sp)
{
    PolizElem *tmp=(*sp)->pointer;
    PolizItem *p=(*sp);
    (*sp)=(*sp)->next;
    delete p;
    return tmp;
}

//PolizJump methods
void PolizJump::Evaluate(PolizItem **ip, PolizItem **sp) const
{
    PolizLabel *l=dynamic_cast<PolizLabel *>(Pop(sp));
    if (!l)
        throw (Exception("label not found (jump)"));
    (*ip)=l->Get();
    delete l;
}

void PolizJump::Text() const
{
    printf(" ! ");
}

//PolizJumpFalse methods
void PolizJumpFalse::Evaluate(PolizItem **ip, PolizItem **sp) const
{
    PolizLabel *l=dynamic_cast<PolizLabel *>(Pop(sp));
    if (!l)
        throw (Exception("label not found (jump false)"));
    PolizInt *x=dynamic_cast<PolizInt *>(Pop(sp));
    if (!x)
        throw (Exception("int not found (jump false)"));
    if (!x->Get())
        (*ip)=l->Get();
    else
        (*ip)=(*ip)->next;
    delete x;
    delete l;
}

void PolizJumpFalse::Text() const
{
    printf(" !F ");
}

//PolizRegular methods
void PolizRegular::Evaluate(PolizItem **ip, PolizItem **sp) const
{
    EvalReg(sp);
    (*ip)=(*ip)->next;
}

//PolizNihil methods
void PolizNihil::Text() const
{
    printf(" NIHIL ");
}

//PolizConst methods
void PolizConst::EvalReg(PolizItem **sp) const
{
    Push(sp, Clone());
}

//PolizVaraddr methods
PolizVarAddr::PolizVarAddr(char *str)
{
    int len=strlen(str);
    name=new char[len];
    strcpy(name,str);
}

char * PolizVarAddr::Get() const
{
    return name;
}

PolizElem * PolizVarAddr::Clone() const
{
    return new PolizVarAddr(name);
}

void PolizVarAddr::Text() const
{
    printf(" (%s) ", name);
}

//PolizLabel methods
PolizLabel::PolizLabel(PolizItem *ap)
{
    p=ap;
    name=NULL;
    lt=NULL;
}

PolizLabel::PolizLabel(char *str, LabelTable *t)
{
    lt=t;
    p=NULL;
    int len=strlen(str);
    name=new char[len];
    strcpy(name,str);
}

PolizItem * PolizLabel::Get() const
{
    return p;
}

PolizElem * PolizLabel::Clone() const
{
    if (p)
        return new PolizLabel(p);
    else
        return new PolizLabel(getlab(lt, name));
}

void PolizLabel::Text() const
{
    if (name)
        printf(" %s ", name);
    else
        printf(" <label> ");
}

//PolizString methods
PolizString::PolizString(char *as)
{
    int len=strlen(as);
    s=new char[len];
    strcpy(s,as);
}

char * PolizString::Get() const
{
    return s;
}

PolizElem * PolizString::Clone() const
{
    return new PolizString(s);
}

void PolizString::Text() const
{
    printf("%s\n", s);
}

//PolizInt methods
PolizInt::PolizInt(int an): n(an) {}

int PolizInt::Get() const
{
    return n;
}

PolizElem * PolizInt::Clone() const
{
    return new PolizInt(n);
}

void PolizInt::Text() const
{
    printf("%d\n", n);
}

//PolizValue methods
PolizValue::PolizValue(VarTable *p)
{
    vt=p;
}

void PolizValue::EvalReg(PolizItem **sp) const
{
    PolizVarAddr *var=dynamic_cast<PolizVarAddr *>(Pop(sp));
    if (!var)
        throw (Exception("variable not found (value)"));
    Push(sp, new PolizInt(getval(vt, var->Get())));
}

void PolizValue::Text() const
{
    printf(" ; ");
}

//PolizFunc1 methods
void PolizFunc1::EvalReg(PolizItem **sp) const
{
    PolizInt *op=dynamic_cast<PolizInt *>(Pop(sp));
    if (!op)
        throw (Exception("int not found (func1)"));
    Execute(sp, op->Get());
}

//PolizFunc2 methods
void PolizFunc2::EvalReg(PolizItem **sp) const
{
    PolizInt *op1=dynamic_cast<PolizInt *>(Pop(sp));
    if (!op1)
        throw(Exception("int not found (func2, op1)"));
    PolizInt *op2=dynamic_cast<PolizInt *>(Pop(sp));
    if (!op2)
        throw(Exception("int not found (func2, op2)"));
    Execute(sp, op1->Get(), op2->Get());
}

//Functions without arguments
void PolizTurn::EvalReg(PolizItem **sp) const
{
    c->SendCmd(turn);
    c->m.turned=true;
}

void PolizTurn::Text() const
{
    printf(" turn ");
}

void PolizMyId::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->me.n));
}

void PolizMyId::Text() const
{
    printf(" ?my_id ");
}

void PolizMyMoney::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->me.money));
}

void PolizMyMoney::Text() const
{
    printf(" ?my_money ");
}

void PolizMyStuff::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->me.stuff));
}

void PolizMyStuff::Text() const
{
    printf(" ?my_stuff ");
}

void PolizMyProd::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->me.prod));
}

void PolizMyProd::Text() const
{
    printf(" ?my_prod ");
}

void PolizMyFactories::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->me.fact));
}

void PolizMyFactories::Text() const
{
    printf(" ?my_factories ");
}

void PolizPlayers::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->m.npl));
}

void PolizPlayers::Text() const
{
    printf(" ?players ");
}

void PolizMarketStuff::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->m.stuff));
}

void PolizMarketStuff::Text() const
{
    printf(" ?market_stuff ");
}

void PolizMarketMinPrice::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->m.minprice));
}

void PolizMarketMinPrice::Text() const
{
    printf(" ?market_min_price ");
}

void PolizMarketProd::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->m.prod));
}

void PolizMarketProd::Text() const
{
    printf(" ?market_prod ");
}

void PolizMarketMaxPrice::EvalReg(PolizItem **sp) const
{
    Push(sp, new PolizInt(c->m.maxprice));
}

void PolizMarketMaxPrice::Text() const
{
    printf(" ?market_max_price ");
}

void PolizPrint::EvalReg(PolizItem **sp) const
{
    PolizElem *arg=Pop(sp);
    arg->Text();
}

void PolizPrint::Text() const
{
    printf(" print ");
}

PolizIdx::PolizIdx(VarTable *p)
{
    vt=p;
}

void PolizIdx::EvalReg(PolizItem **sp) const
{
    PolizVarAddr *va;
    char *buf=new char[LEN];
    for (int i=0; i<LEN; i++)
        buf[i]=0;
    PolizInt *idx=dynamic_cast<PolizInt *>(Pop(sp));
    if (!idx)
        throw(Exception("int not found (idx)"));
    PolizElem *tmp=Pop(sp);
    va=dynamic_cast<PolizVarAddr *>(tmp);
    if (!va)
        throw(Exception("var not found (idx)"));
    sprintf(buf, "%s[%d]", va->Get(), idx->Get());
    Push(sp, new PolizVarAddr(buf));
    delete va;
    delete idx;
}

void PolizIdx::Text() const
{
    printf(" [] ");
}

PolizAssign::PolizAssign(VarTable *p)
{
    vt=p;
}

void PolizAssign::EvalReg(PolizItem **sp) const
{
    PolizInt *op=dynamic_cast<PolizInt *>(Pop(sp));
    if (!op)
        throw(Exception("int not found (assign)"));
    PolizVarAddr *var=dynamic_cast<PolizVarAddr *>(Pop(sp));
    if (!var)
        throw(Exception("var not found (assign)"));
    asgn(vt, var->Get(), op->Get());
}

void PolizAssign::Text() const
{
    printf(" := ");
}
//Functions with 1 argument
void PolizUnaryPlus::Execute(PolizItem **sp, int op) const
{
    Push(sp, new PolizInt(op));
}

void PolizUnaryPlus::Text() const
{
    printf(" (+) ");
}

void PolizUnaryMinus::Execute(PolizItem **sp, int op) const
{
    Push(sp, new PolizInt(-op));
}

void PolizUnaryMinus::Text() const
{
    printf(" (-) ");
}

void PolizNot::Execute(PolizItem **sp, int op) const
{
    Push(sp, new PolizInt(!op));
}

void PolizNot::Text() const
{
    printf(" not ");
}

void PolizProdCmd::Execute(PolizItem **sp, int op) const
{
    c->SendCmd(prod, op);
}

void PolizProdCmd::Text() const
{
    printf(" prod ");
}

void PolizBuild::Execute(PolizItem **sp, int op) const
{
    for (int i=0; i<op; i++);
    c->SendCmd(build);
}

void PolizBuild::Text() const
{
    printf(" build ");
}

void PolizMoney::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].money));
    else
        Push(sp, new PolizInt(-1));
}

void PolizMoney::Text() const
{
    printf(" ?money ");
}

void PolizStuff::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].stuff));
    else
        Push(sp, new PolizInt(-1));
}

void PolizStuff::Text() const
{
    printf(" ?stuff ");
}

void PolizProdFunc::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].prod));
    else
        Push(sp, new PolizInt(-1));
}

void PolizProdFunc::Text() const
{
    printf(" ?prod ");
}

void PolizFactory::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].fact));
    else
        Push(sp, new PolizInt(-1));
}

void PolizFactory::Text() const
{
    printf(" ?factories ");
}

void PolizResultSold::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].sellres.items));
    else
        Push(sp, new PolizInt(-1));
}

void PolizResultSold::Text() const
{
    printf(" ?result_sold ");
}

void PolizResultProdPrice::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].sellres.price));
    else
        Push(sp, new PolizInt(-1));
}

void PolizResultProdPrice::Text() const
{
    printf(" ?result_prod_price ");
}

void PolizResultBought::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].buyres.items));
    else
        Push(sp, new PolizInt(-1));
}

void PolizResultBought::Text() const
{
    printf(" ?result_bought ");
}

void PolizResultStuffPrice::Execute(PolizItem **sp, int op) const
{
    if (op>=1&&op<=c->m.npl)
        Push(sp, new PolizInt(c->m.plist[op-1].buyres.price));
    else
        Push(sp, new PolizInt(-1));
}

void PolizResultStuffPrice::Text() const
{
    printf(" ?result_stuff_price ");
}

//Functions with 2 arguments
void PolizPlus::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op1+op2));
}

void PolizPlus::Text() const
{
    printf(" + ");
}

void PolizMinus::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2-op1));
}

void PolizMinus::Text() const
{
    printf(" - ");
}

void PolizMul::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op1*op2));
}

void PolizMul::Text() const
{
    printf(" * ");
}

void PolizDiv::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2/op1));
}

void PolizDiv::Text() const
{
    printf(" / ");
}

void PolizMod::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2%op1));
}

void PolizMod::Text() const
{
    printf(" %% ");
}

void PolizPower::Execute(PolizItem **sp, int op1, int op2) const
{
    if (op1>=0&&op2>=0)
        Push(sp, new PolizInt(power(op2,op1)));
    else
        throw(Exception("wrong arguments for ^", 0));
}

void PolizPower::Text() const
{
    printf(" ^ ");
}

void PolizEq::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op1==op2));
}

void PolizEq::Text() const
{
    printf(" = ");
}

void PolizGreater::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2>op1));
}

void PolizGreater::Text() const
{
    printf(" > ");
}

void PolizGreaterEqual::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2>=op1));
}

void PolizGreaterEqual::Text() const
{
    printf(" >= ");
}

void PolizLess::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2<op1));
}

void PolizLess::Text() const
{
    printf(" < ");
}

void PolizLessEqual::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2<=op1));
}

void PolizLessEqual::Text() const
{
    printf(" <= ");
}

void PolizAnd::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2&&op1));
}

void PolizAnd::Text() const
{
    printf(" & ");
}

void PolizOr::Execute(PolizItem **sp, int op1, int op2) const
{
    Push(sp, new PolizInt(op2||op1));
}

void PolizOr::Text() const
{
    printf(" | ");
}

void PolizBuy::Execute(PolizItem **sp, int op1, int op2) const
{
    c->SendCmd(buy, op2, op1);
}

void PolizBuy::Text() const
{
    printf(" buy ");
}

void PolizSell::Execute(PolizItem **sp, int op1, int op2) const
{
    c->SendCmd(sell, op2, op1);
}

void PolizSell::Text() const
{
    printf(" sell ");
}

