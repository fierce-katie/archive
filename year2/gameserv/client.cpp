#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "client.h"

bool Client::MakeSocket(char * const *argv)
{
    int port;
    struct sockaddr_in addr;
    if (!sscanf(argv[2], "%d", &port)) {
        printf("Wrong port number\n");
        return false;
    }
    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);
    if (!inet_aton(argv[1], &(addr.sin_addr))) {
        printf("Wrong ip\n");
        return false;
    }
    sockfd=socket(AF_INET, SOCK_STREAM, 0);
    b.sockfd=sockfd;
    if (connect(sockfd, (struct sockaddr *) &addr, sizeof(addr))) {
        printf("Connection failed\n");
        return false;
    }
    return true;
}

void Client::SendCmd(enum command cmd, int arg1, int arg2)
{
    if (m.turned)
        return;
    if (connected) {
        char buf[64];
        for (int i=0; i<64; i++)
            buf[i]=0;
        switch (cmd) {
            case market:
                sprintf(buf, "market\n\r");
                break;
            case player:
                if (arg1)
                    sprintf(buf, "player %d\n\r", arg1);
                else
                    sprintf(buf, "player\n\r");
                break;
            case prod:
                sprintf(buf, "prod %d\n\r", arg1);
                break;
            case buy:
                sprintf(buf, "buy %d %d\n\r", arg1, arg2);
                break;
            case sell:
                sprintf(buf, "sell %d %d\n\r", arg1, arg2);
                break;
            case build:
                sprintf(buf, "build\n\r");
                break;
            case turn:
                sprintf(buf, "turn\n\r");
                break;
            case start:;
        }
        write(sockfd, buf, strlen(buf));
    }
}

char * Client::Buf::TakeStr(int l)
{
    char *tmp=(char *)malloc(l);
    for (int i=0; i<l-1; i++)
        tmp[i]=buf[i];
    tmp[l-1]=0;
    return tmp;
}

char * Client::Buf::MyStrChr(const char c)
{
    char *tmp=NULL;
    if (len>0)
        for (tmp=buf; (*tmp!=c) && (tmp!=buf+len-1); tmp++) {}
    return tmp;
}

char * Client::Buf::GetStr()
{
    while (true) {
        int n=0;
        if (MyStrChr('\n')==NULL)
            if (!(n=read(sockfd, buf+len, 512)))
                return NULL;
        len+=n;
        char *endl;
        int l;
        while ((endl=MyStrChr('\n'))!=NULL) {
            char *info=NULL;
            l=endl-buf+1;
            if (buf[0]=='>' || buf[0]=='*' || buf[0]=='%')
                info=TakeStr(l);
            memmove(buf, buf+l, len-l);
            len-=l;
            for (int i=len; i<1024; i++)
                buf[i]=0;
            if (info)
                return info;
        }
    }
}

void Client::AddRes(char *info)
{
    int n, i, p;
    if (info[0]=='*') {
        sscanf(info+2, "%d %d %d", &n, &i, &p);
        m.plist[n-1].buyres.items=i;
        m.plist[n-1].buyres.price=p;
        // printf("Player %d bought %d for %d\n", n, i, p);
    } else {
        sscanf(info+1, "%d %d %d", &n, &i, &p);
        m.plist[n-1].sellres.items=i;
        m.plist[n-1].sellres.price=p;
        // printf("Player %d sold %d for %d\n", n, i, p);
    }
}

void Client::GetInfo(enum command cmd, Player &pl)
{
    if (!connected)
        return;
    char *info;
    info=b.GetStr();
    if (!info) {
        connected=false;
        return;
    }
    switch (cmd) {
        case market:
            sscanf(info+1, "%d %d %d %d", &m.stuff, &m.minprice,
                    &m.prod, &m.maxprice);
            free(info);
            break;
        case player:
            sscanf(info+1, "%ld %d %d %d", &pl.money, &pl.stuff,
                    &pl.prod, &pl.fact);
            free(info);
            break;
        case start:
            sscanf(info+1, "%d %d", &pl.n, &m.npl);
            break;
        case turn:
            for (int i=0; i<m.npl; i++) {
                m.plist[i-1].buyres.items=m.plist[i-1].buyres.price=0;
                m.plist[i-1].sellres.items=m.plist[i-1].sellres.price=0;
            }
            while (strcmp(info, "%")) {
                AddRes(info);
                if((info=b.GetStr())==NULL) {
                    connected=false;
                    return;
                }
            }
            break;
        case prod: case buy: case sell: case build:;
    }
}

void Client::NewInfo()
{
    m.turned=false;
    SendCmd(player);
    GetInfo(player, me);
    //printf("\nPlayer: %ld %d %d %d\n\n", me.money, me.stuff, me.prod, me.fact);
    SendCmd(market);
    GetInfo(market, me);
    //printf("Market: %d %d %d %d\n\n", m.stuff, m.minprice, m.prod, m.maxprice);
    for (int i=0; i<m.npl; i++) {
        SendCmd(player, i+1);
        GetInfo(player, m.plist[i]);
        //printf("Player %d: %ld %d %d %d\n\n", i+1, m.plist[i].money,
        //                                    m.plist[i].stuff,
        //                                    m.plist[i].prod, m.plist[i].fact);
    }
}

void Client::StartGame()
{
    GetInfo(start, me);
    //printf("Me=%d, players=%d\n", me.n, m.npl);
    m.plist=(Player *)malloc(m.npl*sizeof(me));
    me.buyres.items=me.buyres.price=0;
    me.sellres.items=me.sellres.price=0;
    for (int i=0; i<m.npl; i++) {
        m.plist[i].n=i+1;
        m.plist[i].buyres.items=m.plist[i].buyres.price=0;
        m.plist[i].sellres.items=m.plist[i].sellres.price=0;
    }
    NewInfo();
}

void Client::End() const
{
    int tmp[128];
    while (read(sockfd, tmp, 128)) {}
    shutdown(sockfd, 2);
    close(sockfd);
}
