#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/uio.h>
#include <time.h>
#define MAX 1
#define MIN 2
#define STUFF 1
#define PROD 2

enum command {
    market,
    player,
    prod,
    buy,
    sell,
    build,
    turn,
    help,
    err
};

enum status {
    off,
    writing,
    complete,
    undef,
    turned,
    loser,
    disconnected
};

struct auction {
    int npl;
    int n;
    int price;
    struct auction *next;
};

struct player {
    int fd;
    int n;
    char buf[1024];
    int buflen;
    char cmd[256];
    int arg1;
    int arg2;
    enum status stat;
    long int money;
    int stuff;
    int prod;
    int toprod;
    int factory;
    int *building;
    int nbuilding;
    struct player *next;
    int sell;
    int buy;
};

struct level {
    int n;
    int nstuff;
    int pmin;
    int nprod;
    int pmax;
};

struct banker {
    int month;
    struct level lvl;
    int active;
    int started;
    int maxplayers;
    struct auction *buy;
    struct auction *sell;
    struct player *plist;
};

/*Make listening socket*/
int mkls(int p)
{
    int l, opt=1;
    struct sockaddr_in addr;
    addr.sin_family=AF_INET;
    addr.sin_port=htons(p);
    addr.sin_addr.s_addr=INADDR_ANY;
    l=socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(l, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    if (bind(l, (struct sockaddr *) &addr, sizeof(addr))) {
        perror("bind");
        return 0;
    }
    if (listen(l, 5)<0) {
        perror("listen");
        return 0;
    }
    return l;
}

/*Check attributes*/
int check(int npl, int port)
{
    if (npl<=0 || port<=1023 || port>=65535) {
        printf("Wrong number of players or port number.\n");
        return 1;
    }
    return 0;
}

/*Bank initialization*/
struct banker initbank(int max)
{
    struct banker b;
    b.month=1;
    b.active=0;
    b.started=0;
    b.lvl.n=3;
    b.lvl.nstuff=max*2;
    b.lvl.pmin=500;
    b.lvl.nprod=max*2;
    b.lvl.pmax=5500;
    b.maxplayers=max;
    b.sell=NULL;
    b.buy=NULL;
    b.plist=NULL;
    return b;
}

/*Add player's descriptors to set*/
fd_set addfds(struct player *pl, fd_set set, int *max)
{
    while (pl!=NULL) {
        if (pl->stat!=off&&pl->stat!=disconnected) {
            *max=(pl->fd>*max)?(pl->fd):(*max);
            FD_SET(pl->fd, &set);
        }
        pl=pl->next;
    }
    return set;
}

/*Broadcast WAKEUP*/
void wakeup(struct player *pl)
{
    const char wakestr[15] = "\nWake up!\n";
    while (pl!=NULL) {
        if (pl->stat!=off&&pl->stat!=disconnected)
            write(pl->fd, wakestr, strlen(wakestr)+1);
        pl=pl->next;
    }
}

/*Free players list*/
void freelist(struct player *pl)
{
    struct player *tmp;
    while (pl!=NULL) {
        tmp=pl->next;
        free(pl->building);
        free(pl);
        pl=tmp;
    }
}

/*Free auction list*/
struct auction *freelist2(struct auction *l)
{
    struct auction *tmp;
    while (l!=NULL) {
        tmp=l->next;
        free(l);
        l=tmp;
    }
    return NULL;
}

/*Check if all players turn*/
int allturn(struct player *pl)
{
    while(pl!=NULL) {
        if (pl->stat==writing||pl->stat==complete||pl->stat==undef)
            return 0;
        pl=pl->next;
    }
    return 1;
}

/*Change market level*/
struct level changelvl(struct level lvl, int l, int pl)
{
    lvl.n=l;
    switch (l) {
        case 1:
            lvl.nstuff=pl;
            lvl.pmin=800;
            lvl.nprod=3*pl;
            lvl.pmax=6500;
            break;
        case 2:
            lvl.nstuff=(int)(1.5*pl);
            lvl.pmin=650;
            lvl.nprod=(int)(2.5*pl);
            lvl.pmax=6000;
            break;
        case 3:
            lvl.nstuff=2*pl;
            lvl.pmin=500;
            lvl.nprod=2*pl;
            lvl.pmax=5500;
            break;
        case 4:
            lvl.nstuff=(int)(2.5*pl);
            lvl.pmin=400;
            lvl.nprod=(int)(1.5*pl);
            lvl.pmax=5000;
            break;
        case 5:
            lvl.nstuff=3*pl;
            lvl.pmin=300;
            lvl.nprod=pl;
            lvl.pmax=4500;
    }
    return lvl;
}

/*Choose next level*/
struct level nextlevel(int current, int pl)
{
    int l=0, r, sum=0;
    struct level lvl;
    const int lvlchange[5][5]={
        {4,4,2,1,1},
        {3,4,3,1,1},
        {1,3,4,3,1},
        {1,1,3,4,3},
        {1,1,2,4,4}
    };
    //srand(time(NULL));
    r=1+rand()%12;
    while (sum<r) {
        sum+=lvlchange[current-1][l];
        l++;
    }
    return changelvl(lvl, l, pl);
}

/*Count connected players*/
int connected(struct player *pl)
{
    int n=0;
    while (pl!=NULL) {
        if (pl->stat!=off)
            n++;
        pl=pl->next;
    }
    return n;
}

/*Help*/
void printhelp(int fd)
{
    const char str1[25]="\nCommands avaliable:\n";
    const char cmds[8][80]= {
        "  market - current market info,\n",
        "  player n - info about player n,\n",
        "  prod n - produce n items,\n",
        "  buy n p - stuff auction, n items, p - purchasing price,\n",
        "  sell n p - production auction, n intems, p - price,\n",
        "  build - build new factory,\n",
        "  turn - end of move,\n",
        "  help - list of possible commands.\n"
    };
    int i;
    write(fd, str1, strlen(str1)+1);
    for (i=0; i<8; i++)
        write(fd, cmds[i], strlen(cmds[i])+1);
}

/*Add player to list*/
struct player *addplayer(struct player *pl, int fd)
{
    struct player *tmp;
    int i;
    tmp=pl;
    if (pl==NULL) {
        pl=malloc(sizeof(*pl));
        tmp=pl;
    } else {
        while (tmp->next!=NULL)
            tmp=tmp->next;
        tmp->next=malloc(sizeof(*tmp));
        tmp=tmp->next;
    }
    tmp->fd=fd;
    tmp->n=0;
    for (i=0; i<1024; i++)
        (tmp->buf)[i]=0;
    tmp->buflen=0;
    for (i=0; i<256; i++)
        tmp->cmd[i]=0;
    tmp->arg1=tmp->arg2=0;
    tmp->stat=undef;
    tmp->money=10000;
    tmp->stuff=4;
    tmp->prod=2;
    tmp->toprod=0;
    tmp->factory=2;
    tmp->building=NULL;
    tmp->nbuilding=0;
    tmp->next=NULL;
    tmp->sell=0;
    tmp->buy=0;
    printhelp(fd);
    return pl;
}

/*Print start info*/
void startpl(struct player *pl, int max)
{
    int i;
    const char startstr[50]="\n---THE BATTLE BEGINS---\n\n---Month 1---\n";
    char tmpbuf[64];
    for (i=0; i<64; i++)
        tmpbuf[i]=0;
    i=1;
    while (pl!=NULL) {
        if (pl->stat!=off) {
            pl->n=i;
            write(pl->fd, startstr, strlen(startstr)+1);
            sprintf(tmpbuf, "\nYour number   Total number\n>%10d %13d\n", i, max);
            write(pl->fd, tmpbuf, strlen(tmpbuf)+1);
            i++;
        }
        pl=pl->next;
    }
}

/*Event at listening socket*/
struct player *newplayer(int ls, struct player *pl, struct banker *b)
{
    struct player *tmp;
    int fd;
    const char startedstr[40]="\nThe game has already started.\n";
    const char newstr[30]="\nNew player joined.\n";
    char statsstr[60];
    for (fd=0; fd<60; fd++)
        statsstr[fd]=0;
    fd=accept(ls, NULL, NULL);
    if (b->started) {
        write(fd, startedstr, strlen(startedstr)+1);
        shutdown(fd, 2);
        close(fd);
    } else {
        pl=addplayer(pl, fd);
        printf("New player at %d.\n", fd);
        tmp=pl;
        sprintf(statsstr ,"\nConnected players: %d, waiting for %d more.\n",
                connected(pl), (b->maxplayers)-connected(pl));
        while (tmp!=NULL) {
            write(tmp->fd, newstr, strlen(newstr)+1);
            write(tmp->fd, statsstr, strlen(statsstr)+1);
            tmp=tmp->next;
        }
    }
    if (connected(pl)==b->maxplayers) {
        b->started=1;
        b->active=b->maxplayers;
        b->plist=pl;
        printf("All players connected.\n");
        startpl(pl, b->maxplayers);
    }
    return pl;
}

/*Add new bid to auction list*/
struct auction *addbid(struct auction *list, int player, int n, int price)
{
    struct auction *tmp;
    tmp=list;
    if (list==NULL) {
        list=malloc(sizeof(*list));
        tmp=list;
    } else {
        while (tmp->next!=NULL)
            tmp=tmp->next;
        tmp->next=malloc(sizeof(*tmp));
        tmp=tmp->next;
    }
    tmp->npl=player;
    tmp->n=n;
    tmp->price=price;
    tmp->next=NULL;
    return list;
}

/*Get command code*/
enum command getcmd(char* buf)
{
    if (!strcmp(buf, "market"))
        return market;
    if (!strcmp(buf, "player"))
        return player;
    if (!strcmp(buf, "prod"))
        return prod;
    if (!strcmp(buf, "buy"))
        return buy;
    if (!strcmp(buf, "sell"))
        return sell;
    if (!strcmp(buf, "build"))
        return build;
    if (!strcmp(buf, "turn"))
        return turn;
    if (!strcmp(buf, "help"))
        return help;
    return err;
}

/*Print market info*/
void marketcmd(struct player *pl, struct banker *b)
{
    const char wrongstr[25]="\nWrong command syntax.\n";
    char tmpbuf[1024];
    const char statstr[30]="\nMarket info:\n";
    int i;
    for (i=0; i<1024; i++)
        tmpbuf[i]=0;
    if (pl->arg1||pl->arg2) {
        write(pl->fd, wrongstr, strlen(wrongstr)+1);
        return;
    }
    else {
        write(pl->fd, statstr, strlen(statstr)+1);
        sprintf(tmpbuf, "\nCurrent level %d\n"
                "   Stuff  Min price  Prod  Max price\n"
                ">  %5d  %9d  %4d  %9d\n",
                b->lvl.n, b->lvl.nstuff, b->lvl.pmin, b->lvl.nprod,
                b->lvl.pmax);
        write(pl->fd, tmpbuf, strlen(tmpbuf)+1);
    }
}

/*Print player info*/
void playercmd(struct player *p, struct player *pl)
{
    const char wrongstr[25]="\nWrong command syntax.\n";
    const char nopl[25]="\nPlayer not found.\n";
    char tmpbuf[1024];
    int i;
    for (i=0; i<1024; i++)
        tmpbuf[i]=0;
    if (p->arg2) {
        write(p->fd, wrongstr, strlen(wrongstr)+1);
        return;
    }
    if (!p->arg1)
        p->arg1=p->n;
    do {
        if (p->arg1==pl->n) {
            sprintf(tmpbuf, "\nPlayer %d:\n"
                    "   Money  Stuff  Products  Factories\n"
                    "> %6ld  %5d  %8d  %9d\n",
                    pl->n, pl->money, pl->stuff,
                    pl->prod, pl->factory);
            write(p->fd, tmpbuf, strlen(tmpbuf)+1);
            return;
        }
        pl=pl->next;
    } while (pl!=NULL);
    write(p->fd, nopl, strlen(nopl)+1);
}

/*Produce*/
void prodcmd(struct player *pl)
{
    const char wrongstr[25]="\nWrong command syntax.\n";
    const char errstr[20]="\nInvalid command.\n";
    const char valstr[50]="\nYou don't have enough resourses.\n";
    const char prodstr[25]="\nProduction started.\n";
    if (pl->arg1<=0||pl->arg2) {
        write(pl->fd, wrongstr, strlen(wrongstr)+1);
        return;
    }
    if (pl->stat==loser||pl->stat==turned) {
        write(pl->fd, errstr, strlen(errstr)+1);
        return;
    }
    if (pl->arg1>pl->factory || pl->arg1>pl->stuff) {
        write(pl->fd, valstr, strlen(valstr)+1);
        return;
    }
    (pl->toprod)+=pl->arg1;
    (pl->stuff)-=pl->arg1;
    (pl->money)-=2000*(pl->arg1);
    write(pl->fd, prodstr, strlen(prodstr)+1);
}

/*Stuff auction bid*/
void buycmd(struct player *pl, struct banker *b)
{
    const char wrongstr[25]="\nWrong command syntax.\n";
    const char errstr[20]="\nInvalid command.\n";
    const char itemstr[20]="\nToo many items.\n";
    const char pricestr[30]="\nPrice is too low.\n";
    const char accptstr[20]="\nBid accepted.\n";
    if (pl->arg1<=0||pl->arg2<=0) {
        write(pl->fd, wrongstr, strlen(wrongstr)+1);
        return;
    }
    if (pl->stat==loser||pl->stat==turned||pl->buy) {
        write(pl->fd, errstr, strlen(errstr)+1);
        return;
    }
    if (pl->arg1>b->lvl.nstuff) {
        write(pl->fd, itemstr, strlen(itemstr)+1);
        return;
    }
    if (pl->arg2<b->lvl.pmin) {
        write(pl->fd, pricestr, strlen(pricestr)+1);
        return;
    }
    pl->buy=1;
    b->buy=addbid(b->buy, pl->n, pl->arg1, pl->arg2);
    write(pl->fd, accptstr, strlen(accptstr)+1);
}

/*Prod auction bid*/
void sellcmd(struct player *pl, struct banker *b)
{
    const char wrongstr[25]="\nWrong command syntax.\n";
    const char errstr[20]="\nInvalid command.\n";
    const char itemstr[20]="\nToo many items.\n";
    const char pricestr[30]="\nPrice is too high.\n";
    const char accptstr[20]="\nBid accepted.\n";
    if (pl->arg1<=0||pl->arg2<=0) {
        write(pl->fd, wrongstr, strlen(wrongstr)+1);
        return;
    }
    if (pl->stat==loser||pl->stat==turned||pl->sell) {
        write(pl->fd, errstr, strlen(errstr)+1);
        return;
    }
    if (pl->arg1>b->lvl.nprod||pl->arg1>pl->prod) {
        write(pl->fd, itemstr, strlen(itemstr)+1);
        return;
    }
    if (pl->arg2>b->lvl.pmax) {
        write(pl->fd, pricestr, strlen(pricestr)+1);
        return;
    }
    pl->sell=1;
    b->sell=addbid(b->sell, pl->n, pl->arg1, pl->arg2);
    write(pl->fd, accptstr, strlen(accptstr)+1);
}

/*Build new factory*/
void buildcmd(struct player *pl, int m)
{
    const char wrongstr[25]="\nWrong command syntax.\n";
    const char errstr[20]="\nInvalid command.\n";
    const char buildstr[30]="\nBuilding new factory.\n";
    if (pl->arg1||pl->arg2) {
        write(pl->fd, wrongstr, strlen(wrongstr)+1);
        return;
    }
    if (pl->stat==loser||pl->stat==turned) {
        write(pl->fd, errstr, strlen(errstr)+1);
        return;
    }
    pl->building=realloc(NULL, sizeof(int));
    pl->building[pl->nbuilding++]=m;
    pl->money-=2500;
    write(pl->fd, buildstr, strlen(buildstr)+1);
}

/*End of move*/
void turncmd(struct player *pl)
{
    const char movestr[40]="\nYour move is over.\n";
    const char errstr[20]="\nInvalid command.\n";
    if (pl->stat!=loser) {
        pl->stat=turned;
        write(pl->fd, movestr, strlen(movestr)+1);
    } else
        write(pl->fd, errstr, strlen(errstr)+1);
}

/*Analyse player's command*/
void move(struct player *pl, char *str, struct banker *b)
{
    enum command c;
    int i;
    const char errstr[20]="\nInvalid command.\n";
    if (!sscanf(str, "%s %d %d", pl->cmd, &(pl->arg1), &(pl->arg2))) {
        write(pl->fd, errstr, strlen(errstr)+1);
        return;
    }
    c=getcmd(pl->cmd);
    for (i=0; i<256; i++)
        pl->cmd[i]=0;
    switch (c) {
        case market:
            marketcmd(pl, b);
            break;
        case player:
            playercmd(pl, b->plist);
            break;
        case prod:
            prodcmd(pl);
            break;
        case buy:
            buycmd(pl, b);
            break;
        case sell:
            sellcmd(pl, b);
            break;
        case build:
            buildcmd(pl, b->month);
            break;
        case turn:
            turncmd(pl);
            break;
        case help:
            printhelp(pl->fd);
            break;
        case err:
            write(pl->fd, errstr, strlen(errstr)+1);
    }
}

char *mystrchr(char *str, int len, const char c)
{
    char *tmp=NULL;
    if (strlen(str))
        for (tmp=str; (*tmp!=c) && (tmp!=str+len-1); tmp++) {}
    return tmp;
}

char *takestr(char *str, int l)
{
    char *tmp=malloc(l);
    int i;
    for (i=0; i<l-1; i++)
        tmp[i]=str[i];
    tmp[l-1]=0;
    return tmp;
}

/*Read command*/
struct player *readcmd(struct player *pl, struct banker *b)
{
    //char tmpbuf[1024];
    const char rpt[60]="\nError reading your command. Try again.\n";
    const char waitstr[60]="\nWait for the other players.\n";
    int k, nrcv;
    char *endl, *tmp;
    //for (i=0; i<1024; i++)
    //tmpbuf[i]=0;
    if (pl->stat!=turned&&pl->stat!=loser)
        pl->stat=writing;
    nrcv=read(pl->fd, pl->buf+pl->buflen, 1024);
    pl->buflen+=nrcv;
    switch (nrcv) {
        case -1:
            if (b->started)
                write(pl->fd, rpt, strlen(rpt)+1);
            else
                write(pl->fd, waitstr, strlen(waitstr)+1);
            break;
        case 0:
            pl->stat=(b->started)?disconnected:off;
            shutdown(pl->fd, 2);
            close(pl->fd);
            printf("Player at %d is off.\n", pl->fd);
            break;
        default:
            if (b->started) {
                while ((endl=mystrchr(pl->buf, pl->buflen, '\n'))!=NULL) {
                    k=endl-pl->buf+1;
                    tmp=takestr(pl->buf, k);
                    memmove(pl->buf, pl->buf+k, pl->buflen-k);
                    pl->buflen-=k;
                    for (k=pl->buflen; k<1024; k++)
                        pl->buf[k]=0;
                    if (pl->stat==writing||pl->stat==turned||pl->stat==loser) {
                        move(pl, tmp, b);
                        free(tmp);
                        pl->arg1=0;
                        pl->arg2=0;
                    }
                }
                /* i=strlen(pl->buf);
                   for (k=0; k<strlen(tmpbuf); k++)
                   if ((pl->buf[i++]=tmpbuf[k])=='\n') {
                   pl->buf[i-1]=0;
                   if (pl->stat!=turned&&pl->stat!=loser)
                   pl->stat=complete;
                   break;
                   }*/
            } else
                write(pl->fd, waitstr, strlen(waitstr)+1);
    }
    return pl;
}

/*Check events at players' descriptors*/
struct player *event(struct player *pl, fd_set readfds, struct banker *b)
{
    struct player *tmp;
    tmp=pl;
    while (tmp!=NULL) {
        if (FD_ISSET(tmp->fd, &readfds))
            tmp=readcmd(tmp, b);
        tmp=tmp->next;
    }
    return pl;
}

/*Month results*/
void newmonth(struct player *pl, int month)
{
    int i, k;
    const char completestr[30]="\nNew factory completed.\n";
    char tmpbuf[30];
    for (i=0; i<30; i++)
        tmpbuf[i]=0;
    while (pl!=NULL) {
        if (pl->stat==loser) {
            pl=pl->next;
            continue;
        }
        pl->money-=300*(pl->stuff)+500*(pl->prod)+1000*(pl->factory);
        pl->prod+=(pl->toprod);
        if (pl->toprod) {
            sprintf(tmpbuf, "\n%d items produced.\n", pl->toprod);
            write(pl->fd, tmpbuf, strlen(tmpbuf)+1);
        }
        pl->toprod=0;
        k=0;
        for (i=0; i<pl->nbuilding; i++)
            if (pl->building[i]==month-5) {
                pl->building[i]=0;
                k++;
                write(pl->fd, completestr, strlen(completestr)+1);
            }
        pl->factory+=k;
        pl->money-=2500*k;
        pl->buy=0;
        pl->sell=0;
        pl=pl->next;
    }
}

/*Check for new losers*/
void losers(struct player *pl)
{
    const char losestr[30]="\n---YOU FAILED---\n";
    char tmpbuf[50];
    struct player *first, *tmp;
    int i;
    for (i=0; i<50; i++)
        tmpbuf[i]=0;
    first=pl;
    while (pl!=NULL) {
        if (pl->stat==loser)
            pl->money=0;
        if ((pl->money<=0&&pl->stat!=loser)||pl->stat==disconnected) {
            if (pl->stat!=disconnected) {
                pl->stat=loser;
                write(pl->fd, losestr, strlen(losestr)+1);
            } else
                pl->stat=off;
            sprintf(tmpbuf, "\nPlayer %d went bankrupt.\n", pl->n);
            tmp=first;
            while (tmp!=NULL) {
                if (tmp->fd!=pl->fd&&pl->stat!=off)
                    write(tmp->fd, tmpbuf, strlen(tmpbuf)+1);
                tmp=tmp->next;
            }
            printf("Player %d went bankrupt.\n", pl->n);
        }
        pl=pl->next;
    }
}

/*Sort auction list*/
struct auction *sortlist(struct auction *list, int mode)
{
    struct auction *cur, *first, *run, tmp;
    first=list;
    while (first!=NULL) {
        cur=run=first;
        while (run!=NULL) {
            if (mode==MAX&&run->price>cur->price)
                cur=run;
            if (mode==MIN&&run->price<cur->price)
                cur=run;
            run=run->next;
        }
        tmp.npl=first->npl;
        first->npl=cur->npl;
        cur->npl=tmp.npl;
        tmp.n=first->n;
        first->n=cur->n;
        cur->n=tmp.n;
        tmp.price=first->price;
        first->price=cur->price;
        cur->price=tmp.price;
        first=first->next;
    }
    return list;
}

/*Buy from player*/
void buyfrom(int player, int items, int price, struct banker *b)
{
    char tmpbuf[50];
    int i;
    struct player *tmp;
    for (i=0; i<50; i++)
        tmpbuf[i]=0;
    sprintf(tmpbuf, "   Player  Items  Price\n>%8d %6d %6d\n",
            player, items, price);
    tmp=b->plist;
    while (tmp!=NULL) {
        if (tmp->stat==turned)
            write(tmp->fd, tmpbuf, strlen(tmpbuf)+1);
        tmp=tmp->next;
    }
    tmp=b->plist;
    while (tmp->n!=player)
        tmp=tmp->next;
    tmp->prod-=items;
    tmp->money+=price*items;
}

/*Sell to player*/
void sellto(int player, int items, int price, struct banker *b)
{
    char tmpbuf[50];
    int i;
    struct player *tmp;
    for (i=0; i<50; i++)
        tmpbuf[i]=0;
    sprintf(tmpbuf, "   Player  Items  Price\n*%8d %6d %6d\n",
            player, items, price);
    tmp=b->plist;
    while (tmp!=NULL) {
        if (tmp->stat==turned)
            write(tmp->fd, tmpbuf, strlen(tmpbuf)+1);
        tmp=tmp->next;
    }
    tmp=b->plist;
    while (tmp->n!=player)
        tmp=tmp->next;
    tmp->stuff+=items;
    tmp->money-=price*items;
}

/*Chose random players for auction*/
void lot(struct auction *first, struct auction *last, int item,
        struct banker *bank, int mode)
{
    int end=0, n=0, player, i;
    struct auction *run;
    run=first;
    while (run!=last) {
        n++;
        run=run->next;
    }
    while (item) {
        end=1;
        run=first;
        while (run!=last) {
            if (run->npl!=0) {
                end=0;
                break;
            }
            run=run->next;
        }
        if (end)
            break;
        run=first;
        //srand(time(NULL));
        player=1+rand()%n;
        for (i=0; i<player-1; i++)
            run=run->next;
        if (run->npl) {
            if (mode==PROD)
                buyfrom(run->npl, ((run->n)>item)?item:(run->n), run->price, bank);
            else
                sellto(run->npl, ((run->n)>item)?item:(run->n), run->price, bank);
            run->npl=0;
            if (item>run->n)
                item-=run->n;
            else
                item=0;
        }
    }
}

/*Product auction*/
void prodauction(struct banker *bank)
{
    struct auction *cur, *run;
    int price, n, prod;
    struct player *tmp;
    const char resstr[30]="\nProduct auction results:\n";
    tmp=bank->plist;
    while (tmp!=NULL) {
        if (tmp->stat==turned)
            write(tmp->fd, resstr, strlen(resstr)+1);
        tmp=tmp->next;
    }
    cur=bank->sell;
    prod=bank->lvl.nprod;
    while (prod) {
        price=cur->price;
        n=0;
        run=cur;
        while (run!=NULL&&run->price==price) {
            n+=run->n;
            run=run->next;
        }
        if (n<=prod) {
            prod-=n;
            while (cur!=run) {
                buyfrom(cur->npl, cur->n, cur->price, bank);
                cur=cur->next;
            }
        } else {
            lot(cur, run, prod, bank, PROD);
            prod=0;
        }
        if (run==NULL)
            break;
    }
}

/*Stuff auction*/
void stuffauction(struct banker *bank)
{
    struct auction *cur, *run;
    int price, n, stuff;
    struct player *tmp;
    const char resstr[30]="\nStuff auction results:\n";
    tmp=bank->plist;
    while (tmp!=NULL) {
        if (tmp->stat==turned)
            write(tmp->fd, resstr, strlen(resstr)+1);
        tmp=tmp->next;
    }
    cur=bank->buy;
    stuff=bank->lvl.nstuff;
    while (stuff) {
        price=cur->price;
        n=0;
        run=cur;
        while (run!=NULL&&run->price==price) {
            n+=run->n;
            run=run->next;
        }
        if (n<=stuff) {
            stuff-=n;
            while (cur!=run) {
                sellto(cur->npl, cur->n, cur->price, bank);
                cur=cur->next;
            }
        } else {
            lot(cur, run, stuff, bank, STUFF);
            stuff=0;
        }
        if (run==NULL)
            break;
    }
}

/*Get month results*/
void endofmonth(struct player *players, struct banker *bank)
{
    printf("End of month.\n");
    bank->sell=sortlist(bank->sell, MIN);
    bank->buy=sortlist(bank->buy, MAX);
    losers(players);
    if (bank->sell!=NULL)
        prodauction(bank);
    if (bank->buy!=NULL)
        stuffauction(bank);
    printf("Auction ended.\n");
    bank->month++;
    bank->sell=freelist2(bank->sell);
    bank->buy=freelist2(bank->buy);
    newmonth(players, bank->month);
    losers(players);
}

/*Count active players*/
int active(struct player *pl)
{
    int i=0;
    while (pl!=NULL) {
        if (pl->stat==turned) {
            pl->stat=undef;
            i++;
        }
        pl=pl->next;
    }
    return i;
}

/*No winners*/
void nowinner(struct player *pl)
{
    const char endstr[50]="\n---NO WINNERS---\n\n---END OF GAME---\n";
    while (pl!=NULL) {
        if (pl->stat==turned||pl->stat==loser)
            write(pl->fd, endstr, strlen(endstr)+1);
        pl->stat=loser;
        pl=pl->next;
    }
}

/*Print new month info*/
void newinfo(struct player *pl, struct banker b)
{
    char tmpbuf[20];
    int i;
    for (i=0; i<20; i++)
        tmpbuf[i]=0;
    sprintf(tmpbuf, "\n---Month %d---\n%%\n", b.month);
    while (pl!=NULL) {
        if (pl->stat==undef||pl->stat==loser)
            write(pl->fd, tmpbuf, strlen(tmpbuf)+1);
        pl=pl->next;
    }
}

/*Get winner's number*/
int findwinner(struct player *pl)
{
    while (pl!=NULL) {
        if (pl->stat==undef)
            return pl->n;
        pl=pl->next;
    }
    return 0;
}

/*Winner found*/
void winner(int w, struct player *pl)
{
    const char winstr[20]="\n---YOU WIN---\n";
    const char endstr[20]="\n---END OF GAME---\n";
    char tmpbuf[25];
    int i;
    for (i=0; i<25; i++)
        tmpbuf[i]=0;
    sprintf(tmpbuf, "\n---PLAYER %d WON---\n", w);
    while (pl!=NULL) {
        if (pl->stat==loser) {
            write(pl->fd, tmpbuf, strlen(tmpbuf)+1);
            write(pl->fd, endstr, strlen(endstr)+1);
        }
        if (pl->stat==undef) {
            write(pl->fd, winstr, strlen(winstr)+1);
            write(pl->fd, endstr, strlen(endstr)+1);
        }
        pl=pl->next;
    }
}

/*Print game results*/
void printres(struct player *pl)
{
    while (pl!=NULL) {
        if (pl->stat==undef||pl->stat==loser)
            printf("\nPlayer %d:\nMoney %ld\nStuff %d\nProducts %d\n"
                    "Factories %d\n",pl->n, pl->money,
                    pl->stuff, pl->prod, pl->factory);
        pl=pl->next;
    }
}

/*MAIN LOOP*/
void mainloop(int ls, int npl)
{
    struct banker bank;
    struct player *players=NULL;
    int maxd;
    fd_set readfds;
    struct timeval tv;
    bank=initbank(npl);
    for (;;) {
        tv.tv_sec=300;
        tv.tv_usec=0;
        FD_ZERO(&readfds);
        maxd=ls;
        readfds=addfds(players, readfds, (int *) &maxd);
        FD_SET(ls, &readfds);
        switch (select(maxd+1, &readfds, NULL, NULL, (struct timeval *) &tv)) {
            case -1:
                perror("select");
                break;
            case 0:
                if (bank.started)
                    wakeup(players);
                else {
                    printf("Timeout.");
                    return;
                }
                break;
            default:
                if (FD_ISSET(ls, &readfds))
                    players=newplayer(ls, players, (struct banker*) &bank);
                players=event(players, readfds, (struct banker*) &bank);
        }
        if (allturn(players)&&bank.started==1) {
            endofmonth(players, (struct banker *) &bank);
            bank.active=active(players);
            bank.lvl=nextlevel(bank.lvl.n, bank.active);
            printres(bank.plist);
            printf("Active %d.\n", bank.active);
            if (bank.active==0) {
                printf("No winner.\n");
                nowinner(bank.plist);
                bank.started=-1;
            }
            if (bank.active==1) {
                printf("\nWinner is %d.\n", findwinner(players));
                winner(findwinner(players), bank.plist);
                bank.started=-1;
            }
            if (bank.started==1) {
                printf("New month.\n");
                newinfo(players, bank);
                continue;
            }
        }
        if (bank.started==-1) {
            printf("\nGame over.\n");
            freelist(players);
            return;
        }
    }
}

int main(int argc, char **argv)
{
    int npl, port, ls;
    if (argc!=3) {
        printf("Wrong starting attributes.\n");
        return 1;
    }
    sscanf(argv[1], "%d", &npl);
    sscanf(argv[2], "%d",&port);
    if (check(npl, port))
        return 1;
    if ((ls=mkls(port))<=0)
        return 1;
    printf("Waiting for players.\n");
    mainloop(ls, npl);
    printf("\nSession ended.\n");
    return 0;
}
