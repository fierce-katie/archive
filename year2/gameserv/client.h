#ifndef CLIENT_INCLUDED
#define CLIENT_INCLUDED

enum command {
    market,
    player,
    prod,
    buy,
    sell,
    build,
    turn,
    start,
};

struct Auction {
    int items;
    int price;
};

struct Player {
    int n;
    long int money;
    int stuff;
    int prod;
    int fact;
    Auction buyres;
    Auction sellres;
};

struct Market {
    int npl;
    int stuff;
    int minprice;
    int prod;
    int maxprice;
    bool turned;
    struct Player *plist;
};

class Client {
    int sockfd;
    bool connected;
    bool MakeSocket(char * const *argv);
    class Buf {
        friend class Client;
        char buf[1024];
        int len;
        int sockfd;
        Buf() { len=0; for (int i=0; i<1024; i++) buf[i]=0; };
        char *GetStr();
        char *TakeStr(int len);
        char *MyStrChr(char c);
    };
    friend class Buf;
    Buf b;
    public:
    Player me;
    Market m;
    Client(char * const *argv) { connected=MakeSocket(argv); }
    void StartGame();
    bool GetStat() const { return connected; }
    void NewInfo();
    void SendCmd(enum command cmd, int arg1=0, int arg2=0);
    void GetInfo(enum command cmd, Player &pl);
    void AddRes(char *info);
    void End() const;
};

#endif
