;1 NOT --
;2 AND &
;3 OR V
;4 IMP ->

;Some useful uncategorized things

(setq opSymb '(-- & V ->))

(defun filter (l f)
    (cond
        ((null l) NIL)
        ((funcall f (car l)) (cons (car l) (filter (cdr l) f)))
        (T (filter (cdr l) f))
    )
)

(defun flatten (l)
    (cond
        ((null l) 'FALSE)
        (T (flatten2 l))
    )
)

(defun flatten2 (l)
    (cond
        ((atom l) l)
        ((atom (car l)) (cons (car l) (flatten2 (cdr l))))
        (T (append (flatten2 (car l)) (flatten2 (cdr l))))
    )
)

;Functions to work with sets

;Check if x is member of l, but use certain function to compare
(defun member2 (x l f)
    (cond
        ((null l) NIL)
        ((funcall f x (car l)) l)
        (T (member2 x (cdr l) f))
    )
)

;Make set from list using member2
(defun setFromList (l f)
    (cond
        ((null l) NIL)
        ((member2 (car l) (cdr l) f) (setFromList (cdr l) f))
        (T (cons (car l) (setFromList (cdr l) f)))
    )
)

;Check equality of sets
(defun eqSet (x y f)
    (and (subset x y f) (subset y x f))
)

;Check if x is subset of y
(defun subset (x y f)
    (cond
        ((null x) T)
        ((member2 (car x) y f) (subset (cdr x) y f))
        (T NIL)
    )
)

;Checker-functions for expressions of certain structure

;Check if the expression is a variable
(defun isVar (x)
    (cond
        ((atom x) (null (member x opSymb)))
        (T (and (= 2 (length x)) (equal '-- (car x)) (isVar (second x))))
    )
)

;Check if the expression is a conjunction
(defun isConj (x)
    (cond
        ((isVar x) T)
        ((atom x) NIL)
        (T
            (eval
                (cons
                    'and
                    (mapcar #'(lambda (y) (or (equal '& y) (isVar y))) x)
                )
            )
        )
    )
)

;Functions to simplify expressions of certain structure

;Simplify a variable
(defun simplifyVar (x)
    (cond
        ((atom x) x)
        (T
            (cond
                ((atom (second x))
                    (cond
                        ((equal x '(-- TRUE)) 'FALSE)
                        ((equal x '(-- FALSE)) 'TRUE)
                        (T x)
                    )
                )
                (T (simplifyVar (second (second x))))
            )
        )
    )
)

(defun simp (expr)
    (mapcar
        #'(lambda (x)
            (cond
                ((equal x 'V) x)
                (T (funcall 'simplifyConj x))
            )
         )
         expr
    )
)

(defun notvars (l)
    (mapcar 'second (filter l 'listp))
)

(defun addSym (l sym)
    (cond
        ((null l) NIL)
        ((null (cdr l)) l)
        (T (cons (car l) (cons sym (addSym (cdr l) sym))))
    )
)

(defun makeVarSet (l c)
    (remove c
        (setFromList
            (filter l 'isVar)
            'equal
        )
    )
)

(defun makeConjSet (l)
    (remove 'FALSE
        (setFromList
           (filter l 'isConj)
           'equal
        )
    )
)

(defun intersect (x y)
    (cond
        ((null x) NIL)
        ((member (car x) y) T)
        (T (intersect (cdr x) y))
    )
)

;Simplify a conjunction
(defun simplifyConj (x)
    (cond
        ((isVar x) x)
        (T (let ((vars (makeVarSet x 'TRUE)))
               (cond
                   ((member 'FALSE vars) 'FALSE)
                   ((and (notvars vars) (intersect vars (notvars vars) ))
                       'FALSE
               )
                  (T
                      (cond
                          ((null (cdr vars)) (car vars))
                          (T (addSym vars '&))
                       )
                   )
               )
           )
        )
    )
)

;Simplify a disjunction
(defun simplifyDisj (x)
    (let ((conjs (makeConjSet x)) (vars (makeVarSet x 'FALSE)))
        (cond
            ((member 'TRUE conjs) 'TRUE)
            ((and (notvars vars) (subset (notvars vars) vars 'equal)) 'TRUE)
            (T
                (cond
                    ((null (cdr conjs)) (car conjs))
                    (T (addSym conjs 'V))
                )
           )
        )
    )
)

;Simplify an expression
(defun simple (expr)
    (cond
        ((isVar expr) expr)
        ((isConj expr) (simplifyConj expr))
        (T (simplifyDisj (simp expr)))
    )
)

;Functions to simplify any expression

(defun simplifyExpr (expr)
    (flatten (simple (step3 (step2 (step1 expr)))))
)

(defun step1 (l)
    (unpackVars (impl (packNot l) NIL))
)

(defun step2 (l)
    (unpackDisj (unpack (upNot (packConj l NIL NIL))))
)

(defun step3 (l)
    (unpackDisj (unpack (mult l)))
)

;Packing

(defun packNot (l)
    (cond
        ((atom l) l)
        ((equal '-- (car l))
            (cons (list '-- (packNot (second l))) (packNot (cddr l)))
        )
        (T (cons (packNot (car l)) (packNot (cdr l))))
    )
)

(defun packConj (l acc flag)
    (cond
        ((null l)
            (cond
                (flag (list acc))
                (T acc)
            )
        )
        ((isVar l) l)
        ((isVar (car l)) (packConj (cdr l) (append acc (list (car l))) flag))
        ((atom (car l))
            (cond
                ((equal (car l) '&) (packConj (cdr l) (append acc '(&)) T))
                ((equal (car l) 'V)
                    (cond
                        (flag
                            (append
                                (list acc 'V)
                                (packConj (cdr l) NIL NIL)
                             )
                        )
                        (T (append acc (cons 'V (packConj (cdr l) NIL flag))))
                    )
                )
                (T (packConj (cdr l) (append acc (list (car l))) flag))
            )
        )
        (T
            (packConj
                (cdr l)
                (append  acc (list (packConj (car l) NIL NIL)))
                flag
            )
        )
    )
)

;Converting

(defun mult (l)
    (cond
        ((or (isVar l) (atom l)) l)
        ((member '& l)
            (cond
                ((isConj l) l)
                (T (mult2 l NIL))
            )
        )
        (T (mapcar 'mult l))
    )
)

(defun mult2 (l acc)
    (cond
        ((null l) acc)
        ((or (atom (car l)) (isVar (car l)))
            (mult2 (cdr l) (append acc (list (car l))))
        )
        (T
            (let ((x (mult (car l))))
                (cond
                    ((isConj x) (mult2 (cdr l) (append acc x)))
                    (T
                        (cond
                            (acc
                                (mult (cons (multL acc x) (cdr l)))
                            )
                            (T (mult (multR x (cdr l))))
                        )
                    )
                )
            )
        )
    )
)

(defun multL (c d)
    (mapcar
        #'(lambda (x)
            (cond
                ((equal x 'V) 'V)
                (T (append c (list x)))
            )
        )
        d
    )
)

(defun multR (d c)
    (mapcar
        #'(lambda (x)
            (cond
                ((equal x 'V) 'V)
                (T (append (list x) c))
            )
        )
        d
    )
)

(defun impl (l acc)
    (cond
        ((null l) acc)
        ((atom l)  l)
        ((equal (car l) '->)
            (impl
                (append
                    (list
                        (list '-- acc)
                        'V
                        (second l)
                    )
                    (cddr l)
                )
                NIL
            )
        )
        (T (impl (cdr l) (append acc (list (impl (car l) NIL)))))
    )
)

(defun upNot (expr)
    (cond
        ((atom expr) expr)
        ((isVar expr) (simplifyVar expr))
        ((equal (car expr) '--)
            (cons (deMorgan (second expr)) (upNot (cddr expr)))
        )
        (T (cons (upNot (car expr)) (upNot (cdr expr))))
    )
)

(defun deMorgan (expr)
    (cond
        ((null expr) NIL)
        ((isVar expr) (simplifyVar (list '-- expr)))
        ((equal (car expr) '&) (cons 'V (deMorgan (cdr expr))))
        ((equal (car expr) 'V) (cons '& (deMorgan (cdr expr))))
        ((equal (car expr) '--) (upNot (second expr)))
        (T (cons (deMorgan (car expr)) (deMorgan (cdr expr))))
    )
)

;Unpacking

(defun unpackExternal (l)
    (cond
        ((atom l) l)
        ((and (= (length l) 1) (listp (car l))) (unpackExternal (car l)))
        (T l)
    )
)

(defun unpack (l)
    (cond
        ((atom l) l)
        (T (unpackExternal (mapcar 'unpack l)))
    )
)

(defun unpackDisj (l)
    (cond
        ((atom l) l)
        ((isVar l) l)
        ((member 'V l) (unpackDisj2 l))
        (T (mapcar 'unpackDisj l))
    )
)

(defun unpackDisj2 (l)
    (cond
        ((null l) NIL)
        ((or (atom (car l)) (isVar (car l)))
            (cons (car l) (unpackDisj2 (cdr l)))
        )
        (T
            (cond
                ((member 'V (car l))
                    (append
                        (unpackDisj (car l))
                        (unpackDisj (cdr l))
                    )
                )
                (T (cons (unpackDisj (car l)) (unpackDisj (cdr l))))
            )
        )
    )
)




(defun unpackVars (l)
    (cond
        ((atom l) l)
        ((atom (car l)) (cons (car l) (unpackVars (cdr l))))
        (T
            (cond
                ((= (length (car l)) 1)
                    (cond
                        ((atom (caar l)) (cons (caar l) (unpackVars (cdr l))))
                        (T (cons (unpackVars (caar l)) (unpackVars (cdr l))))
                    )
                )
                (T (cons (unpackVars (car l)) (unpackVars (cdr l))))
            )
        )
    )
)


