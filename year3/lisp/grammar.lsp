;FUNCTIONS FOR GRAMMAR PROCESSING

;Makes list of links like (A \b C) for grammar rule A = b C
(defun makeAutomat (l)
    (makeGraph (homeFirst (sortRules (getRules l)) NIL NIL))
)

;Adds final state (S)
(defun makeGraph (l)
    (mapcar #'makeLink l)
)

;Converts (A b) to (A b S), where S is the final state
(defun makeLink (l)
    (cond
        ((= (length l) 2) (list (car l) (cadr l) 'S))
        (T l)
    )
)

;Makes list of graph links for each rule
(defun getRules (l)
    ;;several links can match one rule, so we need mapcan to append them
    (mapcan #'(lambda (x) (divideRules (car x) (cddr x))) l)
)


;Makes separate rules, e.g: (H = a A | b B) => ((H a A) (H b B))
(defun divideRules (left right)
    (cond
        ((null right) NIL)
        (T
            (cons
                (append (list left) (takeNextRule right))
                (divideRules left (cdr (member '\| right)))
            )
        )
    )
)

;Takes first rule before '|' and omits '='
(defun takeNextRule (l)
    (cond
        ((null l) NIL)
        ((eq (car l) '\|) NIL)
        ((eq (car l) '=) (takeNextRule (cdr l)))
        (T (cons (car l) (takeNextRule (cdr l))))
    )
)

;Puts links for the 'home' state in the beginning
(defun homeFirst (l home other)
    (cond
        ((null l) (append home (reverse other)))
        ((char= (coerce (caar l) 'character) #\H)
            (homeFirst (cdr l) (cons (car l) home) other)
        )
        (T (homeFirst (cdr l) home (cons (car l) other)))
    )
)

;Sorts links in the alphabetical order of its first non-terminals
;We need this function to combine links for the same state together
;in case they were in seperate rules, e.g:
;((H \a A) (A \b B) (H \b S)) => ((H \a A) (H \b S) (A \b B))
(defun sortRules (l)
    (sort l #'(lambda (x y) (char< (coerce (car x) 'character)
                                    (coerce (car y) 'character)))
    )
)


;FUNCTIONS FOR DETERMINIZATION

;Predicate that checks whether the FSM is deterministic
(defun determp (a)
    (checkLinks (caar a) a NIL)
)

;For each state  makes list of terminals, checking if any of them repeat
(defun checkLinks (symb a acc)
    (cond
        ((null a) T)
        ((eq symb (caar a))
            (cond
                ((member (cadar a) acc) NIL)
                (T (checkLinks symb (cdr a) (cons (cadar a) acc)))
            )
        )
        (T (checkLinks (caar a) a NIL))
    )
)

;Determines the FSM
(defun determine (a)
    (reverse (auxDetermine a (getTerms a) (list (list 'H)) NIL))
)

;Makes set of terminals
(defun getTerms (a)
    (makeSet (mapcar #'second a))
)

;Main function for determinisation
;a - original links, terms - set of terminals, q - queue of states to be
;processed, res - accumulates the result
(defun auxDetermine (a terms q res)
    (cond
        ((null q) res)
        (T (let*    ((links (makeNewLinks a (car q) terms))
                    (states (getNewStates links))
                    (newres (append links res)))
                (auxDetermine a terms
                    (addNewStates states newres (cdr q)) newres)
            )
        )
    )
)

;Forms new links for each terminal and current fixed state
(defun makeNewLinks (a state terms)
    (mapcan #'(lambda (x) (addNewLinks (filter a state) state NIL x)) terms)
)

;Leaves only the links from states which are the part of current compound state
(defun filter (a s)
    (cond
        ((null a) NIL)
        ((member (caar a) s)
            (cons (car a) (filter (cdr a) s))
        )
        (T (filter (cdr a) s))
    )
)

;Finds new states for each link (grammar rule) and fixed terminal
;rules - list of links, state - current state, res - accumulates the result,
;c - current terminal
(defun addNewLinks (rules state res c)
    (cond
        ((null rules) res)
        ((equal c (cadar rules))
            (addNewLinks (cdr rules) state
                    (newStateInLink (car rules) state res) c)
        )
        (T (addNewLinks (cdr rules) state res c))
    )
)

;Adds new link to result
(defun newStateInLink (rule state res)
    (cond
        ;;res was empty, just add new link
        ((null res) (list (list state (cadr rule) (cddr rule))))
        ;;such link already exists
        ((member (caddr rule) (caddar res)) res)
        ;;form new compound state, eg. ((A) \b (B)) => ((A) \b (B S))
        (T (list (list state (cadr rule) (cons (caddr rule) (caddar res)))))
    )
)

;Gets new states from new links
(defun getNewStates (l)
    (mapcar #'third l)
)

;Adds new state to the queue of states to be processed
;It is added only if it is not already in the queue
(defun addNewStates (states links res)
    (cond
        ((null states) res)
        ((or (myMember (car states) res #'equalSet)
            (myMember (car states) links #'(lambda (x y) (equalSet x (car y)))))
                (addNewStates (cdr states) links res)
        )
        (T (addNewStates (cdr states) links (append res (list (car states)))))
    )
)

;Convert states from lists to strings
(defun statesStr (l)
    (mapcar #'stateToStr l)
)

;Convert each link
(defun stateToStr (l)
    (list (funcall #'toStr (first l)) (second l) (funcall #'toStr (third l)))
)

;Convert state (H A B) => "HAB"
(defun toStr (l)
    (coerce (mapcar (lambda (x) (coerce x 'character)) l) 'string)
)


;USEFUL ADDITIONAL FUNCTIONS

;Like (member), but can use different predicates to compare elements
(defun myMember (x l eqf)
    (cond
        ((null l) NIL)
        ((funcall eqf x (car l)) l)
        (T (myMember x (cdr l) eqf))
    )
)

;Makes set out of list
(defun makeSet (l)
    (cond
        ((null l) NIL)
        ((member (car l) (cdr l)) (makeSet (cdr l)))
        (T (cons (car l) (makeSet (cdr l))))
    )
)

(defun equalSet (x y)
    (and (subSet x y) (subSet y x))
)

(defun subSet (x y)
    (cond
        ((null x) T)
        ((member (car x) y) (subSet (cdr x) y))
        (T NIL)
    )
)

;MAIN FUNCTION

(defun main (gr)
    (let ((automat (makeAutomat gr)))
        (setq aa automat)
        (cond
            ((determp automat)
                (format t "Automat is deterministic.~%")
                (format t "Result:~%")
                automat
            )
            (T
                (format t "Determinization needed:~%")
                (format t "~S~%" automat)
                (format t "Result:~%")
                (statesStr (determine automat))
            )
        )
    )
)


;TESTS

(setq test0 '((H = \a A \| \b B) (A = \b C) (C = \b B \| \a A \| c) (B = \a C)))
(setq test1 '((H = \a A \| \c) (A = \b B \| \a) (H = \d) (B = \a A \| \c)))
(setq test2 '((H = \a A) (A = \a A \| \a) ))
(setq test3 '((H = \a A \| \b B) (A = \a A \| \a) (B = \b H \| \a)))
(setq test4 '((H = \a A \| \b B \| \c C) (A = \a A \| \a B \| \c)
                (B = \b H \| \b) (C = \c C \| \a A)))
(setq test5 '((H = \a A \| \b B \| \a B) (A = \a A \| \a)
                (B = \a B \| \a A \| \b)))
(setq test6 '((H = \a B) (B = \a C \| \d B) (C = \a B) (B = \a D) (D = \d)))
(setq test7 '((H = \a H \| \a B) (B = \b B \| \b C) (C = \a C \| \c)))
(setq test8 '((H = \a A) (A = \b A \| \b B) (B = \a B \| \b S \| \a A)))
