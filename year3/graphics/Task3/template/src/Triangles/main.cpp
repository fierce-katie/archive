//standard libraries
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
using namespace std;

//opengl headers
#include <GL/glew.h>
#include <GL/freeglut.h>

//opengl mathematics
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>


//functions for shader compilation and linking
#include "shaderhelper.h"
//object for drawing
#include "Branch.h"
#include "Leaf.h"
#include "Texture.h"
#define WOOD_LIGHT 0
#define WOOD_DARK 1
#define LEAF_LIGHT 2
#define LEAF_DARK 3
#define LEAF_RED 4
#define SNOW 5
#define GROUND_GREEN 6
#define GROUND_BROWN 7
#define WOOD_SUMMER 8
#define MONTH 9
//model for drawing: a square from two triangles
Branch* pBranch;
Leaf* pLeaf;
MyObject* pGround;

//struct for loading shaders
ShaderProgram shaderProgram;

//window size
int windowWidth = 800;
int windowHeight = 600;

//last mouse coordinates
int mouseX,mouseY;
//flag for mouse motion
bool mouseFlag  = false;

//camera position
glm::vec3 eye(0,20,40);
//reference point position
glm::vec3 cen(0.0,10,0);
//up vector direction (head of observer)
glm::vec3 up(0,1,0);

//matrices
glm::mat4x4 modelMatrix;
glm::mat4x4 modelViewMatrix;
glm::mat4x4 projectionMatrix;
glm::mat4x4 modelViewProjectionMatrix;
glm::mat4x4 normalMatrix;
glm::mat4x4 viewMatrix;
///defines drawing mode
bool useTexture = true;

//names of shader files. program will search for them during execution
//don't forget place it near executable 
char VertexShaderName[] = "Vertex.vert";
char FragmentShaderName[] = "Fragment.frag";


int locMV, locN, locP, texLoc, locFlag, locLight;
glm::vec4 lightLocation(10, 20 ,10, 1);
glm::vec4 lightLocationEye;
//texture identificator
Texture* texId[10];

//class of tree
enum ObjType {
	branch,
	leaf
};

enum Season {
	spring,
	summer,
	autumn,
	winter
};
Season currentSeason = spring;

class Tree;
vector<Tree*> leafv;
int nBranches = 0;
int maxNumBranches;
int nLeaves = 0;
int globalTime = 0;
const float branchWidth = 0.2f;
const float branchLength = 5.0f;
const float leafCoef = 0.2f;
const float childWidthCoef = 0.4f;
const float childLengthCoef = 0.7f;
class Tree {
	MyObject *pObj;
	ObjType type;	
	Tree *parent;
	Tree *children[5];
	float height; //0 to 1
	float angy; //0 to 360
	float angxy; //-90 to -20 & 20 to 90
	glm::mat4x4 currentModelMatrix;
	float lengthCoef; //m_fLength
	float widthCoef; //m_fWidth
	bool shakeFlag;
public:
	Tree(Tree *p, ObjType t, float h): type(t), height(h), drawFlag(true) {
		shakeFlag = true;
		parent = p;
		growCoef = (type==branch)?0.5f:0.2f;
		for (int i=0; i<5; i++)
			children[i] = NULL;
		if (type == branch) {
			pObj = pBranch;
			nBranches++;
			if (!p) { //tunc
				angy = resultAngle = angxy = shakeAngle = 0;
				lengthCoef = widthCoef = 1;
			} else {
				lengthCoef = p->lengthCoef*childLengthCoef;
				widthCoef = p->widthCoef*childWidthCoef;
				angxy = 20 + rand()%40;
				if (rand()%100 < 50)
					angxy = -angxy;
				shakeAngle = 0;
			}
		} else { //leaf
			pObj = pLeaf;
			nLeaves++;
			angxy = 20 + rand()%70;
			if (rand()%100 < 50)
				angxy = -angxy;
			resultAngle = angxy;
			shakeAngle = 0;
		}
		angy = rand()%360;
	}
	~Tree() {}
	float resultAngle;
	float length;
	float width;
	bool drawFlag;
	float growCoef;
	float shakeAngle;
	void Grow() {
		if (type == leaf && growCoef < 1.5)
			growCoef += 0.008;
		if (type == branch)
			growCoef += 0.001;
		for (int i=0; i<5; i++)
			if (children[i])
				children[i]->Grow();
	}
	void AddElems() {
		int start = parent?0:1;
		bool flag = false;
		for (int i=start; i<5; i++) 
			if (!children[i]) {
				flag = true;
				float h = (rand()%10)/10.0f;
				h = (h>0.5f)?0.5f:h;
				children[i] = new Tree(this, branch, (i+h)/5.0f);
			}
		if (flag)
			return;
		for (int i=0; i<5; i++)
			if (children[i])
				children[i]->AddElems();
	}
	void AddLeaves() {
		if (!children[0] && !children[1] && !children[2]
				&& !children[3] && !children[4])
			for (int i=0; i<5; i++) {
				children[i] = new Tree(this, leaf, (i+0.5)/5.0f);
				leafv.push_back(children[i]);
			}
		else
			for (int i=0; i<5; i++) 
				if (children[i])
					children[i]->AddLeaves();
	}
	void Draw() {
		if (!parent) 
			modelMatrix = glm::mat4();
		else {
			modelMatrix = parent->currentModelMatrix;
			modelMatrix = glm::translate(modelMatrix,
				glm::vec3(0.0f, height*parent->length, 0.0f));
		}
		modelMatrix = glm::rotate(modelMatrix, angy,
			glm::vec3(0.0f, 1.0f, 0.0f));
		if (!parent && (currentSeason == summer || currentSeason == winter)) {
			modelMatrix = glm::rotate(modelMatrix, shakeAngle,
				glm::vec3(0.0f, 0.0f, 1.0f));
			if (shakeFlag)
				if (shakeAngle < 2)
					shakeAngle+=0.1;
				else 
					shakeFlag = false;
			else
				if (shakeAngle > -2)
					shakeAngle-=0.1;
				else
					shakeFlag = true;
		}
		modelMatrix = glm::rotate(modelMatrix, angxy, 
			glm::vec3(0.0f, 0.0f, 1.0f));
		currentModelMatrix = modelMatrix;
		if (type == branch) {
			length = lengthCoef*growCoef*branchLength;
			width = widthCoef*growCoef*branchWidth;
			modelMatrix = glm::scale(modelMatrix, 
				glm::vec3(width, length, width));
		} else {
			modelMatrix = glm::scale(modelMatrix, 
				leafCoef*glm::vec3(growCoef, growCoef, growCoef));
		}
		modelViewMatrix = viewMatrix*modelMatrix;
		normalMatrix = glm::inverseTranspose(modelViewMatrix);
		modelViewProjectionMatrix = projectionMatrix*modelViewMatrix;
		if (type == branch) {
			switch (currentSeason) {
				case spring: 
					glBindTexture(GL_TEXTURE_2D, 
						(texId[WOOD_LIGHT])->GetTextureId());
					glUniform1ui(texLoc,(texId[WOOD_LIGHT])->GetTextureId());
				break;
				case summer:
					glBindTexture(GL_TEXTURE_2D, 
						(texId[WOOD_SUMMER])->GetTextureId());
					glUniform1ui(texLoc,(texId[WOOD_SUMMER])->GetTextureId());
				break;
				case autumn: case winter:
					glBindTexture(GL_TEXTURE_2D, 
						(texId[WOOD_DARK])->GetTextureId());
					glUniform1ui(texLoc,(texId[WOOD_DARK])->GetTextureId());
			}
		} else {
			switch (currentSeason) {
				case spring:
					glBindTexture(GL_TEXTURE_2D, 
						(texId[LEAF_LIGHT])->GetTextureId());
					glUniform1ui(texLoc,(texId[LEAF_LIGHT])->GetTextureId());
				break;
				case summer:
					glBindTexture(GL_TEXTURE_2D, 
						(texId[LEAF_DARK])->GetTextureId());
					glUniform1ui(texLoc,(texId[LEAF_DARK])->GetTextureId());
				break;
				case autumn: case winter:
					glBindTexture(GL_TEXTURE_2D, 
						(texId[LEAF_RED])->GetTextureId());
					glUniform1ui(texLoc,(texId[LEAF_RED])->GetTextureId());
			}
		}
		lightLocationEye = viewMatrix*lightLocation;
		glm::vec3 lightLocationEyeTmp(lightLocationEye.x, lightLocationEye.y, lightLocationEye.z);
		glUniform3fv(locLight, 1, glm::value_ptr(lightLocationEyeTmp));
		glUniformMatrix4fv(locMV,1,0,glm::value_ptr(modelViewMatrix));
		glUniformMatrix4fv(locN,1,0,glm::value_ptr(normalMatrix));
		glUniformMatrix4fv(locP,
				1,0,glm::value_ptr(modelViewProjectionMatrix));
		glUniform1i(locFlag,useTexture);
		if (drawFlag)
			pObj->draw();
		for (int i=0; i<5; i++)
			if (children[i])
				children[i]->Draw();
	}	
};
Tree *myTree;
////////////////////////////////////////////////////////
///

void initTexture()
{
	glActiveTexture(GL_TEXTURE0);

	texId[WOOD_LIGHT] = new Texture("wood1.bmp");
	texId[WOOD_SUMMER] = new Texture("wood2.bmp");
	texId[WOOD_DARK] = new Texture("wood3.bmp");
	texId[LEAF_LIGHT] = new Texture("leaf_spring.bmp");
	texId[LEAF_DARK] = new Texture("leaf_summer.bmp");
	texId[LEAF_RED] = new Texture("leaf_autumn.bmp");
	texId[SNOW] = new Texture("snow.bmp");
	texId[GROUND_GREEN] = new Texture("ground1.bmp");
	texId[GROUND_BROWN] = new Texture("ground2.bmp");
	texId[MONTH] = new Texture("month.bmp");
}

/////////////////////////////////////////////////////////////////////
///is called when program starts
void init()
{
	//enable depth test
	glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);
	//initialize shader program
	shaderProgram.init(VertexShaderName,FragmentShaderName);
	//use this shader program
	glUseProgram(shaderProgram.programObject);

	//create new branch
	pBranch = new Branch();
	//fill in data
	pBranch->initData();

	//initialize opengl buffers and variables inside of object
	pBranch->initGLBuffers(shaderProgram.programObject,"pos","nor","tex");

	
	//create new leaf
	pLeaf = new Leaf();
	//fill in data
	pLeaf->initData();
	//initialize opengl buffers and variables inside of object
	pLeaf->initGLBuffers(shaderProgram.programObject,"pos","nor","tex");

	pGround = new MyObject();
	pGround->initData();
	pGround->initGLBuffers(shaderProgram.programObject,"pos","nor","tex");

	//initializa texture
	initTexture();
	currentSeason = spring;
	srand(time(0));
	myTree = new Tree(NULL, branch, 0);
	maxNumBranches = 200 + rand()%200;
}


/////////////////////////////////////////////////////////////////////
///called when window size is changed
void reshape(int width, int height)
{
  windowWidth = width;
  windowHeight = height;
  //set viewport to match window size
  glViewport(0, 0, width, height);
  
  float fieldOfView = 45.0f;
  float aspectRatio = float(width)/float(height);
  float zNear = 0.1f;
  float zFar = 100.0f;
  //set projection matrix
  projectionMatrix = glm::perspective(fieldOfView,aspectRatio,zNear,zFar);
}

void delLeaves() {
	for (unsigned int i=0; i<leafv.size(); i++) 
		if (leafv[i]->drawFlag) {
			if (i+4 < leafv.size()) {
				leafv[i]->drawFlag = false;
				leafv[i+1]->drawFlag = false;
				leafv[i+2]->drawFlag = false;
				leafv[i+3]->drawFlag = false;
				leafv[i+4]->drawFlag = false;
			} else {
				leafv[i]->drawFlag = false;
			}
			break;
		}
}

void resetLeaves() {
	for (unsigned int i=0; i<leafv.size(); i++) {
		leafv[i]->drawFlag = true;
		leafv[i]->growCoef = 0.4f;
	}
}

bool resetFlag;
void seasonFunc() {
	switch (currentSeason) {
		case summer: 
			resetFlag = false;
		break;
		case winter:
		break;
		case autumn:
			delLeaves();
		break;
		case spring:
			if (!resetFlag) {
				resetLeaves();
				resetFlag = true;
			}
			myTree->Grow();
	}		
}
////////////////////////////////////////////////////////////////////
///actions for single frame
void display()
{
  glClearColor(126.0/255.0 , 192.0/255.0 , 238.0/255.0 , 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  //Draw triangle with shaders (in screen coordinates)
  //need to set uniform in modelViewMatrix
  
  glUseProgram(shaderProgram.programObject);
  locMV = glGetUniformLocation(shaderProgram.programObject,"modelViewMatrix");
  locN = glGetUniformLocation(shaderProgram.programObject,"normalMatrix");
  locP = glGetUniformLocation(shaderProgram.programObject,"modelViewProjectionMatrix");
  texLoc = glGetUniformLocation(shaderProgram.programObject,"textureSampler");
  locFlag = glGetUniformLocation(shaderProgram.programObject,"useTexture");
  locLight = glGetUniformLocation(shaderProgram.programObject, "lightPos");

  //we will need this uniform locations to connect them to our variables
  //if there is some problem
  if (locMV<0 || locN<0 || locP<0 || texLoc <0 || locFlag<0 || locLight<0)
  {
	  //not all uniforms were allocated - show blue screen.
	  //check your variables properly. May be there is unused?
	  glClearColor(0,0,1,1);
	  glClear(GL_COLOR_BUFFER_BIT);
	  //end frame visualization
	  glutSwapBuffers();
	  return;
  }

  //camera matrix. camera is placed in point "eye" and looks at point "cen".
	viewMatrix = glm::lookAt(eye,cen,up);

	modelMatrix = glm::mat4();
	modelMatrix = glm::rotate(modelMatrix, -90.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, 200.0f*glm::vec3(1.0f, 1.0f, 1.0f));
	modelMatrix = glm::translate(modelMatrix, glm::vec3(-0.5f, -0.5f, 0.0f));
	modelViewMatrix = viewMatrix*modelMatrix;
	normalMatrix = glm::inverseTranspose(modelViewMatrix);
	modelViewProjectionMatrix = projectionMatrix*modelViewMatrix;
	switch (currentSeason) {
		case spring: case summer:
			glBindTexture(GL_TEXTURE_2D, (texId[GROUND_GREEN])->GetTextureId());
			glUniform1ui(texLoc,(texId[GROUND_GREEN])->GetTextureId());
		break;
		case autumn:
			glBindTexture(GL_TEXTURE_2D, (texId[GROUND_BROWN])->GetTextureId());
			glUniform1ui(texLoc,(texId[GROUND_BROWN])->GetTextureId());
		break;
		case winter:
			glBindTexture(GL_TEXTURE_2D, (texId[SNOW])->GetTextureId());
			glUniform1ui(texLoc,(texId[SNOW])->GetTextureId());
	}
	lightLocationEye = viewMatrix*lightLocation;
	glm::vec3 lightLocationEyeTmp(lightLocationEye.x, lightLocationEye.y, lightLocationEye.z);
	glUniform3fv(locLight, 1, glm::value_ptr(lightLocationEyeTmp));
	glUniformMatrix4fv(locMV,1,0,glm::value_ptr(modelViewMatrix));
	glUniformMatrix4fv(locN,1,0,glm::value_ptr(normalMatrix));
	glUniformMatrix4fv(locP,
			1,0,glm::value_ptr(modelViewProjectionMatrix));
	glUniform1i(locFlag,useTexture);
	pGround->draw();


	/*modelMatrix = glm::mat4();
	modelMatrix = glm::translate(modelMatrix, glm::vec3(10, 20, 0));
	modelMatrix = glm::rotate(modelMatrix, -180.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	modelMatrix = glm::scale(modelMatrix, 10.0f*glm::vec3(1.0f, 1.0f, 1.0f));
	modelViewMatrix = viewMatrix*modelMatrix;
	normalMatrix = glm::inverseTranspose(modelViewMatrix);
	modelViewProjectionMatrix = projectionMatrix*modelViewMatrix;
	glBindTexture(GL_TEXTURE_2D, (texId[MONTH])->GetTextureId());
	glUniform1ui(texLoc,(texId[MONTH])->GetTextureId());
	glUniform3fv(locLight, 1, glm::value_ptr(lightLocationEyeTmp));
	glUniformMatrix4fv(locMV,1,0,glm::value_ptr(modelViewMatrix));
	glUniformMatrix4fv(locN,1,0,glm::value_ptr(normalMatrix));
	glUniformMatrix4fv(locP,
			1,0,glm::value_ptr(modelViewProjectionMatrix));
	pLeaf->draw();
*/
	if (globalTime < 1500) {
		myTree->Grow();
		if (nBranches < maxNumBranches) {
			if (!(globalTime%400))
				myTree->AddElems();
			else {}
		} else 
			if (!nLeaves)    
				myTree->AddLeaves();
			else {}
	} else {
	//now we can start seasons
		if (!(globalTime%500)) 
			currentSeason = static_cast<Season>((currentSeason+1)%4);
		seasonFunc();
	}
	myTree->Draw();
	globalTime++;
	glutSwapBuffers();
  
}

//////////////////////////////////////////////////////////////////////////
///IdleFunction
void update()
{
	//make animation
	glutPostRedisplay();
}


/////////////////////////////////////////////////////////////////////////
///is called when key on keyboard is pressed
///use SPACE to switch mode
///TODO: place camera transitions in this function
void keyboard(unsigned char key, int mx, int my)
{
	float speed = 0.05f;
	glm::vec3 tmpv(0, 0, 0);//for calculations
	glm::vec3 rightDir(1, 0, 0);// ->
	if (key == ' ')
		useTexture = !useTexture;
	if (key == 27) //esc
		exit(0);
	if (key ==  'e')
		for (int i = 0; i < 3; i++) {
			tmpv[i] = cen[i] - eye[i];
			eye[i] += tmpv[i] * speed / 3.0f;
			cen[i] += tmpv[i] * speed / 3.0f;
		}
	if (key == 'q')
		for (int i = 0; i < 3; i++) {
			tmpv[i] = cen[i] - eye[i];
			eye[i] -= tmpv[i] * speed / 3.0f;
			cen[i] -= tmpv[i] * speed / 3.0f;
		}
	if (key == 'h') { //move left
		rightDir = glm::cross(eye - cen, up);
		glm::normalize(rightDir);
		eye += rightDir * speed;
		cen += rightDir * speed;
	}
	if (key == 'j') { //move down
		eye.y -= 3 * speed;
		cen.y -= 3 * speed;
	}
	if (key == 'k') { //move up
		eye.y += 3 * speed;
		cen.y += 3 * speed;
	}
	if (key == 'l') { //move right
		rightDir = glm::cross(eye - cen, up);
		glm::normalize(rightDir);
		eye -= rightDir * speed;
		cen -= rightDir * speed;
	}
}

void mouse(int button, int mode, int posx, int posy)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		mouseX = posx; mouseY = posy;
		if (mode == GLUT_DOWN)
			mouseFlag = true;
		else
			mouseFlag = false;
	}
}


void motion(int posx, int posy)
{
	int deltaX = 0, deltaY = 0;
	float tmpx;
	glm::vec3 tmp;
	if (mouseFlag) {
		deltaX = posx - mouseX;
		deltaY = posy - mouseY;
		if (deltaX != 0) {
			tmp = cen - eye;
			tmpx = deltaX * M_PI / 180.0f / 10.0f;
			cen.z = (float)(eye.z + sin(tmpx) * tmp.x + cos(tmpx) * tmp.z);
			cen.x = (float)(eye.x + cos(tmpx) * tmp.x - sin(tmpx) * tmp.z);
		}
		if (deltaY != 0) {
			cen.y -= deltaY / 10.0f;
		}
	}
	mouseX = posx;
	mouseY = posy;
}
////////////////////////////////////////////////////////////////////////
///this function is used in case of InitializationError
void emptydisplay()
{
}

////////////////////////////////////////////////////////////////////////
///entry point
int main (int argc, char* argv[])
{
  glutInit(&argc, argv);
#ifdef __APPLE__
  glutInitDisplayMode( GLUT_3_2_CORE_PROFILE| GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#else
  glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
  glutInitContextVersion (3, 2);  
  glutInitContextProfile(GLUT_CORE_PROFILE);
  glutInitContextFlags (GLUT_FORWARD_COMPATIBLE);
  glewExperimental = GL_TRUE;
#endif

  glutCreateWindow("Test OpenGL application");
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutReshapeWindow(windowWidth,windowHeight);
  glutIdleFunc(update);
  glutKeyboardFunc(keyboard);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);

  glewInit();
  const char * slVer = (const char *) glGetString ( GL_SHADING_LANGUAGE_VERSION );
  cout << "GLSL Version: " << slVer << endl;

  try
  {
	init();
  }
  catch (const char *str)
  {
	  cout << "Error During Initialiation: " << str << endl;
	  delete pBranch;
	  delete pLeaf;
	  //start main loop with empty screen
	  glutDisplayFunc(emptydisplay);
	  glutMainLoop();
	  return -1;
  }


  try
  {
	glutMainLoop();
  }
  catch (const char *str)
  {
	  cout << "Error During Main Loop: " << str << endl;
  }
  //release memory
  delete pBranch;
  delete pLeaf;

  return 0;
}
