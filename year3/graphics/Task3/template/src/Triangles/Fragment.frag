//shader version
#version 150 core

//mode of drawing
//if is true, then use Texture
//otherwise draw gradient
uniform int useTexture;

//texture object
uniform sampler2D textureSampler;

//retrieve this data form vertex shader
in VertexData
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
} VertexIn;

out vec4 fragColor;

//TODO: you should use VertexIn.normal value to evaluate Phong Lightning for this pixel
// 
uniform vec3 lightPos;

const vec4 Kd = vec4(0.8,0.8,0.8,1.0);
const vec4 Ka = vec4(0.05,0.05,0.05,1);
const vec4 Ks = vec4(0.4,0.4,0.4,0.4);
float Ke = 20;
		
void main()
{
	vec3 light = normalize(lightPos - VertexIn.position);
	vec3 normal = normalize(VertexIn.normal);
	vec3 eye = normalize(VertexIn.position);

	float diffuseIntensity = clamp(max(dot(normal, light), 0.0), 0.0, 1.0);
	vec3 reflection = normalize(reflect(light, normal));
	float specularIntensity = 
		pow(clamp(max(dot(reflection, eye), 0.0), 0.0, 1.0), Ke);
	fragColor = Ka + Kd * diffuseIntensity + Ks * specularIntensity;
	float brightness = 0.3*fragColor.x+0.59*fragColor.y+0.11*fragColor.z;

	if (useTexture>0)
		fragColor = vec4(2.0f*brightness*texture(textureSampler,VertexIn.texcoord.xy).rgb,length(VertexIn.normal)*length(VertexIn.position));
	
}
