#include "Scene.h"
#include <iostream>
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_inverse.hpp"

CScene::CScene(FILE *fd) 
{
	char modelName[256];
	int n = 1;
	L3DS *loader;
	for (int i = 0; i < 256; i++)
		modelName[i] = 0;
		while (fscanf(fd,"%s %d\n", modelName, &n) != EOF) {
			loader = new L3DS(modelName);
		for (int i = 0; i < n; i++) 
			objVector.push_back(Object(loader, n == 1));
		for (int i = 0; i < 256; i++)
			modelName[i] = 0;
		n = 1;
	}
	light = vec3(0, sceneSize, 0);
	/*for (size_t i = 0; i < objVector.size(); i++) {
		cout << "Object = " << i + 1 << endl;
		cout << "Number of meshes in the file = " << objVector[i].model->GetMeshCount() << endl;
		cout << "Number of triangles in the file = " << objVector[i].model->GetMesh(0).GetTriangleCount() << endl;
		cout << objVector[i].isOpt << endl;
	}*/
	createScene();
}


AxisAlignedBox getBox(LMesh m) {
	Vector3 min(m.GetVertex(0).x, m.GetVertex(0).y, m.GetVertex(0).z), max = min;
	LVector4 current;
	for (unsigned int i = 0; i < m.GetVertexCount(); i++) {
		current = m.GetVertex(i);
		min.x = current.x < min.x ? current.x : min.x;
		min.y = current.y < min.y ? current.y : min.y;
		min.z = current.z < min.z ? current.z : min.z;
		max.x = current.x > max.x ? current.x : max.x;
		max.y = current.y > max.y ? current.y : max.y;
		max.z = current.z > max.z ? current.z : max.z;
	}
	return AxisAlignedBox(min, max);
}

LMatrix4 glmtol(mat4x4 const & m)
{
	LMatrix4 result;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.m[i][j] = m[i][j];
	return result;
}

bool CScene::checkBoxes() {
	vector<AxisAlignedBox> tmpBox;
	LMesh mesh;
	mat4x4 model;
	LMatrix4 modelL;
	for (size_t i = 0; i < objVector.size(); i++) {
		mesh = objVector[i].model->GetMesh(0);
		model = objVector[i].translate * objVector[i].rotate * objVector[i].scale;
		modelL = glmtol(model);
		mesh.SetMatrix(modelL);
		mesh.Optimize(oNone);
		tmpBox.push_back(getBox(mesh));
		//cout << tmpBox[i] << endl;
	}
	for (size_t i = 0; i < tmpBox.size(); i++) {
		for (size_t j = 0; j < tmpBox.size(); j++) {
			if (i != j) {
				if (tmpBox[i].contains(tmpBox[j]))
					//cout << "Box " << i << " contains Box " << j << endl;
					return false;
				if (tmpBox[i].intersects(tmpBox[j]))
					//cout << "Box " << i << " intersects Box " << j << endl;
					return false;
			}
		}
	}
	return true;
}

void CScene::setLocRay(vec3 cameraPos) {
	for (size_t i = 0; i < objVector.size(); i++) {
		objVector[i].invrs = objVector[i].rotate * objVector[i].scale;
		objVector[i].trs = objVector[i].translate * objVector[i].invrs;
		if (objVector[i].isOpt) {
			objVector[i].model->GetMesh(0).SetMatrix(glmtol(objVector[i].trs));
			objVector[i].model->GetMesh(0).Optimize(oNone);
		}
		objVector[i].invtrs = inverse(objVector[i].trs);
		objVector[i].invrs = inverse(objVector[i].invrs);
		objVector[i].localRay.m_start = vec3(objVector[i].invtrs * vec4(cameraPos, 1.0f));
		objVector[i].localLight = vec3(objVector[i].invtrs * vec4(light, 1.0f));
	}
}

void CScene::createScene()
{
	LMesh mesh;
	for (size_t i = 0; i < objVector.size(); i++) {
		mesh = objVector[i].model->GetMesh(0);
		objVector[i].box = getBox(mesh);
	}
	//translate, rotate, scale
	/*CAT*/
	objVector[0].translate = translate(objVector[0].translate, vec3(sceneSize / 2, -sceneSize, 0));
	objVector[0].rotate = rotate(objVector[0].rotate, -135.0f, vec3(0, 0, 1.0f));
	objVector[0].rotate = rotate(objVector[0].rotate, 90.0f, vec3(1.0f, 1.0f, 0));
	objVector[0].scale = scale(objVector[0].scale, 10.0f * vec3(1.0f, 1.0f, 1.0f));
	objVector[0].ambient = vec3(0 / 255.0, 238 / 255.0, 118 / 255.0);
	objVector[0].refract = 0;
	objVector[0].reflect = 0;
	/*for (int i = 0; i < 4; i++) {
	for (int j = 0; j < 4; j++)
	cout << objVector[0].translate[i][j] << ' '; cout << endl;
	}*/

	/*CHAIR CHAIR*/
	objVector[1].translate = translate(objVector[1].translate, vec3(3 * sceneSize / 4, -sceneSize, 3 * sceneSize / 4));
	objVector[1].rotate = rotate(objVector[1].rotate, 180.0f, vec3(0, 0, 1.0f));
	objVector[1].rotate = rotate(objVector[1].rotate, -45.0f, vec3(0, 1.0f, 0));
	objVector[1].scale = scale(objVector[1].scale, 1.0f * vec3(1.0f, 1.0f, 1.0f));
	objVector[1].ambient = vec3(150 / 255.0, 150 / 255.0, 150 / 255.0);
	objVector[1].refract = 0;
	objVector[1].reflect = 0;

	objVector[2].translate = translate(objVector[2].translate, vec3(-3 * sceneSize / 4, -sceneSize, 3 * sceneSize / 4));
	objVector[2].rotate = rotate(objVector[2].rotate, 180.0f, vec3(0, 0, 1.0f));
	objVector[2].rotate = rotate(objVector[2].rotate, -135.0f, vec3(0, 1.0f, 0));
	objVector[2].scale = scale(objVector[2].scale, 1.0f * vec3(1.0f, 1.0f, 1.0f));
	objVector[2].ambient = vec3(238 / 255.0, 18 / 255.0, 137 / 255.0);
	objVector[2].refract = 0;
	objVector[2].reflect = 0;

	/*EXTINGUISHER*/
	objVector[3].translate = translate(objVector[3].translate, vec3(-sceneSize / 2, -sceneSize / 2, 3 * sceneSize / 4));
	objVector[3].rotate = rotate(objVector[3].rotate, -90.0f, vec3(1.0f, 0, 0));
	objVector[3].scale = scale(objVector[3].scale, 0.2f * vec3(1.0f, 1.0f, 1.0f));
	objVector[3].ambient = vec3(150 / 255.0, 0, 0);
	objVector[3].refract = 0;
	objVector[3].reflect = 0;

	/*HARP*/
	objVector[4].translate = translate(objVector[4].translate, vec3(sceneSize / 2, -5 * sceneSize / 8, -sceneSize / 2));;
	objVector[4].rotate = rotate(objVector[4].rotate, -90.0f, vec3(1.0f, 0, 0));
	objVector[4].rotate = rotate(objVector[4].rotate, -45.0f, vec3(0, 0, 1.0f));
	objVector[4].scale = scale(objVector[4].scale, 0.3f * vec3(1.0f, 1.0f, 1.0f));
	objVector[4].ambient = vec3(117 / 255.0, 41 / 255.0, 0);
	objVector[4].refract = 0;
	objVector[4].reflect = 0;

	/*VASE*/
	objVector[5].translate = translate(objVector[5].translate, vec3(-3 * sceneSize / 4, -sceneSize, -3 * sceneSize / 4));
	objVector[5].rotate = rotate(objVector[5].rotate, -90.0f, vec3(1.0f, 0, 0));
	objVector[5].scale = scale(objVector[5].scale, 0.2f * vec3(1.0f, 1.0f, 1.0f));
	objVector[5].ambient = vec3(112 / 255.0, 199 / 255.0, 187 / 255.0);
	objVector[5].refract = 0;
	objVector[5].reflect = 0;

	/*MIRROR*/
	objVector[6].translate = translate(objVector[6].translate, vec3(sceneSize / 2, -3 * sceneSize / 4 + 20.0f, -sceneSize + 5.0f));
	objVector[6].rotate = rotate(objVector[6].rotate, -90.0f, vec3(1.0f, 0, 0));
	//objVector[6].rotate = rotate(objVector[6].rotate, 90.0f, vec3(0, 0, 1.0f));
	objVector[6].ambient = vec3(0, 0, 0);
	objVector[6].refract = 0;
	objVector[6].reflect = 1;

	/*SHELF*/
	objVector[7].translate = translate(objVector[7].translate, vec3(sceneSize / 2 + 20.0f, 0, -sceneSize + 20.0f));
	objVector[7].rotate = rotate(objVector[7].rotate, -90.0f, vec3(1.0f, 0, 0));
	objVector[7].rotate = rotate(objVector[7].rotate, -45.0f, vec3(0, 0, 1.0f));
	objVector[7].ambient = vec3(48 / 255.0, 17 / 255.0, 0);
	objVector[7].refract = 0;
	objVector[7].reflect = 0;

	/*CASE*/
	objVector[8].translate = translate(objVector[8].translate, vec3(0, -sceneSize, -3 *sceneSize / 4));
	objVector[8].rotate = rotate(objVector[8].rotate, -90.0f, vec3(1.0f, 0, 0));
	objVector[8].rotate = rotate(objVector[8].rotate, -45.0f, vec3(0, 0, 1.0f));
	objVector[8].scale = scale(objVector[8].scale, 0.07f * vec3(1.0f, 1.0f, 1.0f));
	objVector[8].ambient = vec3(205 / 255.0, 190 / 255.0, 112 / 255.0);
	objVector[8].refract = 0;
	objVector[8].reflect = 0;

	/*TABLE*/
	objVector[9].translate = translate(objVector[9].translate, vec3(sceneSize / 2, -sceneSize, sceneSize / 2));
	objVector[9].rotate = rotate(objVector[9].rotate, -90.0f, vec3(1.0f, 0, 0));
	objVector[9].scale = scale(objVector[9].scale, 0.04f * vec3(1.0f, 1.0f, 1.0f));
	objVector[9].ambient = vec3(48 / 255.0, 17 / 255.0, 0);
	objVector[9].refract = 0;
	objVector[9].reflect = 0;

	/*CRYSTAL*/
	objVector[10].translate = translate(objVector[10].translate, vec3(-sceneSize / 4, -3 * sceneSize / 4, 3 * sceneSize / 4));
	objVector[10].ambient = vec3(0, 0, 0.5f);
	objVector[10].refract = 1.3290f; //(Water, heavy water, ice)
	objVector[10].reflect = 0;


	ceiling.m_vertices.push_back(vec3(sceneSize, sceneSize, sceneSize));
	ceiling.m_vertices.push_back(vec3(sceneSize, sceneSize, -sceneSize));
	ceiling.m_vertices.push_back(vec3(-sceneSize, sceneSize, -sceneSize));
	ceiling.m_vertices.push_back(vec3(-sceneSize, sceneSize, sceneSize));
	ceiling.m_triangles.push_back(vec3(0, 2, 1));
	ceiling.m_triangles.push_back(vec3(3, 2, 0));

	floor.m_vertices.push_back(vec3(sceneSize, -sceneSize, sceneSize));
	floor.m_vertices.push_back(vec3(sceneSize, -sceneSize, -sceneSize));
	floor.m_vertices.push_back(vec3(-sceneSize, -sceneSize, -sceneSize));
	floor.m_vertices.push_back(vec3(-sceneSize, -sceneSize, sceneSize));
	floor.m_triangles.push_back(vec3(0, 2, 1));
	floor.m_triangles.push_back(vec3(3, 2, 0));

	leftWall.m_vertices.push_back(vec3(-sceneSize, -sceneSize, sceneSize));
	leftWall.m_vertices.push_back(vec3(-sceneSize, -sceneSize, -sceneSize));
	leftWall.m_vertices.push_back(vec3(-sceneSize, sceneSize, -sceneSize));
	leftWall.m_vertices.push_back(vec3(-sceneSize, sceneSize, sceneSize));
	leftWall.m_triangles.push_back(vec3(0, 2, 1));
	leftWall.m_triangles.push_back(vec3(3, 2, 0));

	rightWall.m_vertices.push_back(vec3(sceneSize, -sceneSize, sceneSize));
	rightWall.m_vertices.push_back(vec3(sceneSize, -sceneSize, -sceneSize));
	rightWall.m_vertices.push_back(vec3(sceneSize, sceneSize, -sceneSize));
	rightWall.m_vertices.push_back(vec3(sceneSize, sceneSize, sceneSize));
	rightWall.m_triangles.push_back(vec3(0, 2, 1));
	rightWall.m_triangles.push_back(vec3(3, 2, 0));

	backWall.m_vertices.push_back(vec3(-sceneSize, -sceneSize, -sceneSize));
	backWall.m_vertices.push_back(vec3(sceneSize, -sceneSize, -sceneSize));
	backWall.m_vertices.push_back(vec3(sceneSize, sceneSize, -sceneSize));
	backWall.m_vertices.push_back(vec3(-sceneSize, sceneSize, -sceneSize));
	backWall.m_triangles.push_back(vec3(0, 2, 1));
	backWall.m_triangles.push_back(vec3(3, 2, 0));

	/*if (checkBoxes())
		cout << "OK\n";
	else
		cout << "FUCK\n";*/
}
