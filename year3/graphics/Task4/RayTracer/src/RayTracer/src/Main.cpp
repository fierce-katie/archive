#include "Tracer.h"
#include "stdio.h"
#include "omp.h"

int main(int argc, char** argv)
{
	CTracer tracer;

	int xRes = 512;  // Default resolution
	int yRes = 512;
	int ompFlag = 0;

	if (argc == 2) // There is input file in parameters
	{
		FILE* file = fopen(argv[1], "r");
		if (file)
		{
			int xResFromFile = 0;
			int yResFromFile = 0;
			int omp = 0;
			if (fscanf(file, "%d %d %d", &xResFromFile, &yResFromFile, &omp) == 3)
			{
				xRes = xResFromFile;
				yRes = yResFromFile;
				ompFlag = omp;
			}
			else
				printf("Invalid config format! Using default parameters.\r\n");

			fclose(file);
		}
		else
			printf("Invalid config path! Using default parameters.\r\n");
	}
	else
		printf("No config! Using default parameters.\r\n");
	if (ompFlag)
		tracer.RenderImageWOMP(xRes, yRes);
	else
		tracer.RenderImageWOOMP(xRes, yRes);
	tracer.SaveImageToFile("../img/Result.png");
	getchar();
}
