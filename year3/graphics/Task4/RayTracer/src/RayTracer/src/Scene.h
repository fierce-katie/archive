#pragma once

#include "Types.h"
#include "vector"
#include <stdio.h>
#include "glm/glm.hpp"
#include "l3ds\l3ds.h"
#include "CommonMath\AxisAlignedBox.h"

using namespace std;
using namespace glm;
using namespace Common;

struct Object {
	L3DS *model;
	mat4x4 translate, rotate, scale, invrs, trs, invtrs;
	AxisAlignedBox box;
	Object(L3DS *m, bool b) : model(m), translate(), rotate(), scale(), invrs(), trs(), invtrs(), isOpt(b) {};
	Object() {};
	SRay localRay;
	vec3 localLight, ambient;
	float refract;
	bool isOpt, reflect;
};

class CScene
{
  // Set of meshes
	void createScene();
	bool checkBoxes();
public:
	CScene(FILE *fd);
	vector<Object> objVector;
	const int sceneSize = 100;
	MyMesh ceiling, floor, leftWall, rightWall, backWall;
	void setLocRay(vec3 cameraPos);
	vec3 light, diffuseColor, specularColor;
	const int depthMax = 5;
};

LMatrix4 glmtol(mat4x4 const & m);