#pragma once

#include "glm/glm.hpp"
#include "vector"

const float PI = 4 * atan(1.0f);

struct SRay
{
  glm::vec3 m_start;
  glm::vec3 m_dir;
  bool inObj;
  SRay(glm::vec3 start = glm::vec3(0.0, 0.0, 300.0), glm::vec3 dir = glm::vec3(0.0, 0.0, -1.0), bool b = false)
  { 
    m_start = start;
	m_dir = dir;
	inObj = b;
  }
};

struct SCamera
{
  glm::vec3 m_pos;          // Camera position and orientation
  glm::vec3 m_forward;      // Orthonormal basis
  glm::vec3 m_right;
  glm::vec3 m_up;

  glm::vec2 m_viewAngle;    // View angles, rad
  glm::uvec2 m_resolution;  // Image resolution: w, h

  std::vector<glm::vec3> m_pixels;  // Pixel array
  SCamera() { 
    m_pos = glm::vec3(0.0, 0.0, 300.0);
    m_forward = glm::vec3(0.0, 0.0, -1.0);
	m_forward = glm::normalize(m_forward);
    m_up = glm::vec3(0.0, 1.0, 0.0);
    m_right = glm::cross(m_forward, m_up);
    m_viewAngle.x = PI / 3;
	m_viewAngle.y = PI / 3;
  }
};

struct MyMesh
{
	std::vector<glm::vec3> m_vertices;  
	std::vector<glm::vec3> m_triangles;
};