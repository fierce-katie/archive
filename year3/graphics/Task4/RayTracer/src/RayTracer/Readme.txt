Асирян Александр Камоевич, 328 группа
Галкина Екатерина Владимировна, 324 группа
Осипова Екатерина Данииловна, 314 группа
Вершинин Александр Леонидович, 318 группа

Операционная система: Windows 7 x64

Оборудование: 
	Тип ЦП Mobile QuadCore Intel Core i7-3610QM, 2200 MHz (22 x 100)
	Системная память 8091 МБ (DDR3-1600 DDR3 SDRAM)
	Тип ядра ОС Multiprocessor Free (64-bit)
	Видеоадаптер NVIDIA GeForce GT 630M (2048 МБ)

Формат файла настроек: 3 числа, разделённые пробелом: ширина и высота картинки, использование OpenMP (0 или 1)

Время работы программы: 17.504 секунды (512512WOMP.txt); 54.167 секунды (512512WOOMP.txt); 273.873 секунды (20482048WOMP.txt)

Бонусы: параллелизм вычислений (использование OpenMP)

Ссылки на внешние ресурсы:
	Цвета для объектов: http://www.stm.dp.ua/web-design/color-html.php
	Коэффициент преломления: http://www.stm.dp.ua/web-design/color-html.php
	Модель освещения Фонга: http://steps3d.narod.ru/tutorials/lighting-tutorial.html
	Алгоритм трассировки лучей: http://www.ray-tracing.ru/articles164.html, http://courses.graphics.cs.msu.ru/mod/resource/view.php?id=471, консультация, http://courses.graphics.cs.msu.ru/mod/resource/view.php?id=97, условие прошлогоднего задания
	Поиск пересечения луча и треугольника: http://www.ray-tracing.ru/articles164.html, консультация
	Модели: http://archive3d.net/?page=1
