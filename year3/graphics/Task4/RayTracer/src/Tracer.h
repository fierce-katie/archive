#pragma once

#include "glm/glm.hpp"
#include "Types.h"
#include "Scene.h"

#include "string"

typedef enum typeE {SCENE, REALOBJ, DEFAULT};

class CTracer
{
public:
	SRay MakeRay(glm::uvec2 pixelPos);// Create ray for specified pixel
	vec3 TraceRay(SRay ray, int depth); //Trace ray, compute its color
	void RenderImageWOMP(int xRes, int yRes);
	void RenderImageWOOMP(int xRes, int yRes);
	void SaveImageToFile(std::string fileName);
	vec3 rightHalf, upHalf;
	CTracer();
	SCamera m_camera;
	CScene* m_pScene;
	struct HittedObject {
		float distance;
		typeE type;
		int triNum;
		HittedObject() : distance(0), type(DEFAULT), triNum(0) {};
	};
	HittedObject hit(MyMesh mesh, SRay ray);
	HittedObject hit(LMesh& mesh, SRay ray);
	float fresnelReflectance(float ci, float n);
};