#include "Tracer.h"
#include "lodepng.h"
#include <stdio.h>
#include "Scene.h"

using namespace glm;

CTracer::CTracer() 
{
  FILE *fd = fopen("models.txt", "r");
  m_pScene = new CScene(fd);
  if (fd)
    fclose(fd);
}  

SRay CTracer::MakeRay(glm::uvec2 pixelPos)
{
  glm::vec3 rightHalf = m_camera.m_right*tan(m_camera.m_viewAngle.x/2);
  glm::vec3 upHalf = m_camera.m_up*tan(m_camera.m_viewAngle.y/2);
  float x = (pixelPos.x + 0.5)/m_camera.m_resolution.x*2.0 - 1.0;
  float y = (pixelPos.y + 0.5)/m_camera.m_resolution.y*2.0 - 1.0;
  glm::vec3 dir = m_camera.m_forward + rightHalf*x + upHalf*y;
  return SRay(glm::normalize(dir));
}

glm::vec3 CTracer::TraceRay(SRay ray)
{
  vec3 color(0, 1, 0.5);
  return color;
}

void CTracer::RenderImage(int xRes, int yRes)
{
  m_camera.m_resolution = uvec2(xRes, yRes);
  m_camera.m_pixels.resize(xRes * yRes);
  m_camera.m_viewAngle.y = 2*atan((yRes/xRes)*tan(m_camera.m_viewAngle.x/2));

  for(int i = 0; i < yRes; i++)
    for(int j = 0; j < xRes; j++)
    {
      SRay ray = MakeRay(uvec2(j, i));
      m_camera.m_pixels[i * xRes + j] = TraceRay(ray);
    }
}

void CTracer::SaveImageToFile(std::string fileName)
{
	
	int width = m_camera.m_resolution.x;
	int height = m_camera.m_resolution.y;
	
	unsigned char* imageBuffer = new unsigned char[width*height*24];
	
	int i, j;
	int imageDisplacement = 0;
	int textureDisplacement = 0;
	
	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++)
		{
			vec3 color = m_camera.m_pixels[textureDisplacement + j];
			
			imageBuffer[imageDisplacement + j * 3] = clamp(color.r, 0.0f, 1.0f) * 255.0f;
			imageBuffer[imageDisplacement + j * 3 + 1] = clamp(color.g, 0.0f, 1.0f) * 255.0f;
			imageBuffer[imageDisplacement + j * 3 + 2] = clamp(color.b, 0.0f, 1.0f) * 255.0f;
		}
		
		imageDisplacement += width*3;
		textureDisplacement += width;
	}
	
	lodepng_encode24_file(fileName.c_str(), imageBuffer, width, height);
}
