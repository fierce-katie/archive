#include "Scene.h"

CScene::CScene(FILE *fd) 
{
	char modelName[256];
	int n = 1;
	L3DS *loader;
	for (int i = 0; i < 256; i++)
		modelName[i] = 0;
	while (fscanf(fd,"%s %d\n", modelName, &n) != EOF) {
		printf("Loading %s\n", modelName);
		loader = new L3DS(modelName);
		for (int i = 0; i < n; i++) 
			objVector.push_back(Object(loader));
		printf("%d new objects added\n", n);
		for (int i = 0; i < 256; i++)
			modelName[i] = 0;
		n = 1;
	}
	createScene();
}


void CScene::createScene()
{
	//translate, rotate, scale
}
