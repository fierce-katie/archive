#pragma once

#include "glm/glm.hpp"
#include "Types.h"
#include "Scene.h"

class CTracer
{
  SCamera m_camera;
  CScene* m_pScene;
public:
  CTracer();
  SRay MakeRay(glm::uvec2 pixelPos);  // Create ray for specified pixel
  glm::vec3 TraceRay(SRay ray); // Trace ray, compute its color
  void RenderImage(int xRes, int yRes);
  void SaveImageToFile(std::string fileName);
};
