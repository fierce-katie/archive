#pragma once

#include "Types.h"
#include "vector"
#include <stdio.h>
#include "glm/glm.hpp"
#include "l3ds.h"
//#include "CommonMath/inc.h"

using namespace std;
using namespace glm;

struct Object {
	L3DS *model;
	glm::mat4x4 translateMatrix, rotateMatrix, scaleMatrix;
//	AxisAlignedBox box;
	Object(L3DS *m) : model(m) {};
};

class CScene
{
  // Set of meshes
	void createScene();
public:
	CScene(FILE *fd);
	vector<Object> objVector;
};
