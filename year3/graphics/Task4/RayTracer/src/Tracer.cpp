#include "Tracer.h"
#include <stdio.h>
#include "Scene.h"
#include "atlimage.h"
#include <omp.h>
#include "l3ds\l3ds.h"

using namespace glm;

CTracer::CTracer() 
{
    FILE *fd = fopen("models.txt", "r");
    m_pScene = new CScene(fd);
    if (fd)
        fclose(fd);
	m_pScene->setLocRay(m_camera.m_pos);
	m_pScene->diffuseColor = vec3(0.2f, 0.2f, 0.2f);
	m_pScene->specularColor = vec3(0.5f, 0.5f, 0.5f);
	rightHalf = m_camera.m_right * tan(m_camera.m_viewAngle.x / 2);
	upHalf = m_camera.m_up * tan(m_camera.m_viewAngle.y / 2);
}    

SRay CTracer::MakeRay(uvec2 pixelPos)
{
	float x = (pixelPos.x + 0.5f) / m_camera.m_resolution.x * 2.0f - 1.0f;
    float y = (pixelPos.y + 0.5f) / m_camera.m_resolution.y * 2.0f - 1.0f;
    vec3 dir = m_camera.m_forward + rightHalf * x + upHalf * y;
	return SRay(m_camera.m_pos, normalize(dir), false);
}

CTracer::HittedObject CTracer::hit(MyMesh mesh, SRay ray) {
	vec3 e1, e2, p, q, T;
	float coef, u, v, t;
	HittedObject obj;
	for (size_t i = 0; i < mesh.m_triangles.size(); i++) {
		e1 = mesh.m_vertices[int(mesh.m_triangles[i].y)] - mesh.m_vertices[int(mesh.m_triangles[i].x)];
		e2 = mesh.m_vertices[int(mesh.m_triangles[i].z)] - mesh.m_vertices[int(mesh.m_triangles[i].x)];
		T = ray.m_start - mesh.m_vertices[int(mesh.m_triangles[i].x)];
		p = cross(ray.m_dir, e2);
		coef = dot(p, e1);
		u = dot(p, T) / coef;
		if (u < 0)
			continue;
		q = cross(T, e1);
		v = dot(q, ray.m_dir) / coef;
		if (v < 0)
			continue;
		if (u + v > 1)
			continue;
		t = dot(q, e2) / coef;
		if (t < 0)
			continue;
		if ((t < obj.distance) || (obj.type == DEFAULT)) {
			obj.distance = t;
			obj.triNum = i;
			obj.type = REALOBJ;
		}
	}
	return obj;
}

CTracer::HittedObject CTracer::hit(LMesh& mesh, SRay ray) {
	vec3 e1, e2, p, q, T;
	float coef, u, v, t;
	HittedObject obj;
	LTriangle triangle;
	LVector4 a, b, c;
	for (unsigned int i = 0; i < mesh.GetTriangleCount(); i++) {
		triangle = mesh.GetTriangle(i);
		a = mesh.GetVertex(triangle.a);
		b = mesh.GetVertex(triangle.b);
		c = mesh.GetVertex(triangle.c);
		e1 = vec3(b.x - a.x, b.y - a.y, b.z - a.z);
		e2 = vec3(c.x - a.x, c.y - a.y, c.z - a.z);
		T = ray.m_start - vec3(a.x, a.y, a.z);
		p = cross(ray.m_dir, e2);
		coef = dot(p, e1);
		u = dot(p, T) / coef;
		if (u < 0)
			continue;
		q = cross(T, e1);
		v = dot(q, ray.m_dir) / coef;
		if (v < 0)
			continue;
		if (u + v > 1)
			continue;
		t = dot(q, e2) / coef;
		if (t < 0)
			continue;
		if ((t < obj.distance) || (obj.type == DEFAULT)) {
			obj.distance = t;
			obj.triNum = i;
			obj.type = REALOBJ;
			/*break;*/
		}
	}
	return obj;
}

bool rayBoxIntersection(SRay ray, AxisAlignedBox box)
{
	vec3 invDir = 1.0f / ray.m_dir;
	float lo = invDir.x *( box.getMinimum().x - ray.m_start.x);
	float hi = invDir.x *( box.getMaximum().x - ray.m_start.x);
	float tmin = min(lo, hi);
	float tmax = max(lo, hi);
	float lo1 = invDir.y * ( box.getMinimum().y - ray.m_start.y);
	float hi1 = invDir.y * ( box.getMaximum().y - ray.m_start.y);
	tmin = max(tmin, min(lo1, hi1));
	tmax = min(tmax, max(lo1, hi1));
	float lo2 = invDir.z * ( box.getMinimum().z - ray.m_start.z);
	float hi2 = invDir.z * ( box.getMaximum().z - ray.m_start.z);
	tmin = max(tmin, min(lo2, hi2));
	tmax = min(tmax, max(lo2, hi2));
	return (tmin <= tmax) && (tmax > 0.0f);
}

float CTracer::fresnelReflectance(float ci, float n)
{
	float ci2 = ci * ci;
	float si2 = 1.0f - ci2;
	float si4 = si2 * si2;
	float a = ci2 + n * n - 1.0f;
	if (a < 0)
		return 1;
	float sqa = 2 * sqrtf(a) * ci;
	float b = ci2 + a;
	float c = ci2 * a + si4;
	float d = sqa * si2;
	return (b - sqa) / (b + sqa) * (1.0f + (c - d) / (c + d)) * 0.5f;
}

vec3 CTracer::TraceRay(SRay ray, int depth)
{
	vec3 color(0, 191 / 255.0, 255 / 255.0), intPoint, direction, eye, normal, reflect, triangle;
	HittedObject obj, tmpobj;
	MyMesh hitmesh;
	LMesh *hitLmesh, instmesh;
	Object tmp;
	const float shininess = 10.0f;
	vector<int> hitIndex;
	int hitted = -1;
	for (size_t i = 0; i < m_pScene->objVector.size(); i++) {
		tmp = m_pScene->objVector[i];
		tmp.localRay.m_dir = vec3(tmp.invrs * vec4(ray.m_dir, 0.0f));
		tmp.localRay.m_start = vec3(tmp.invtrs * vec4(ray.m_start, 1.0f));
		tmp.localLight = vec3(tmp.invtrs * vec4(m_pScene->light, 1.0f));
		if (rayBoxIntersection(tmp.localRay, tmp.box))
			hitIndex.push_back(i);
	}
	/*for (size_t i = 0; i < hitIndex.size(); i++) {
		tmp = m_pScene->objVector[hitIndex[i]];
		tmpobj = hit(tmp.model->GetMesh(0), tmp.localRay);
		if (tmpobj.type == REALOBJ)
			if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
				obj = tmpobj;
				hitLmesh = &tmp.model->GetMesh(0);
				color = tmp.ambient;
			}
	}*/
	for (size_t i = 0; i < hitIndex.size(); i++) {
		tmp = m_pScene->objVector[hitIndex[i]];
		if (!tmp.isOpt) {
			instmesh = tmp.model->GetMesh(0);
			instmesh.SetMatrix(glmtol(tmp.trs));
			instmesh.Optimize(oNone);
			tmpobj = hit(instmesh, ray);
		}
		else
			tmpobj = hit(tmp.model->GetMesh(0), ray);
		if (tmpobj.type == REALOBJ)
			if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
				obj = tmpobj;
				color = tmp.ambient;
				hitted = hitIndex[i];
				if (!tmp.isOpt)
					hitLmesh = &instmesh;
				else
					hitLmesh = &tmp.model->GetMesh(0);
			}
	}
	tmpobj = hit(m_pScene->backWall, ray);
	if (tmpobj.type == REALOBJ)
		if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
			obj = tmpobj;
			obj.type = SCENE;
			hitmesh = m_pScene->backWall;
			color = vec3(244 / 255.0, 164 / 255.0, 96 / 255.0);
		}
	tmpobj = hit(m_pScene->leftWall, ray);
	if (tmpobj.type == REALOBJ)
		if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
			obj = tmpobj;
			obj.type = SCENE;
			hitmesh = m_pScene->leftWall;
			color = vec3(210 / 255.0, 180 / 255.0, 140 / 255.0);
		}
	tmpobj = hit(m_pScene->rightWall, ray);
	if (tmpobj.type == REALOBJ)
		if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
			obj = tmpobj;
			obj.type = SCENE;
			hitmesh = m_pScene->rightWall;
			color = vec3(210 / 255.0, 180 / 255.0, 140 / 255.0);
		}
	tmpobj = hit(m_pScene->ceiling, ray);
	if (tmpobj.type == REALOBJ)
		if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
			obj = tmpobj;
			obj.type = SCENE;
			hitmesh = m_pScene->ceiling;
			color = vec3(200 / 255.0, 178 / 255.0, 238 / 255.0);
		}
	tmpobj = hit(m_pScene->floor, ray);
	if (tmpobj.type == REALOBJ)
		if ((tmpobj.distance < obj.distance) || (obj.type == DEFAULT)) {
			obj = tmpobj;
			obj.type = SCENE;
			hitmesh = m_pScene->floor;
			color = vec3(0, 0, 0);
		}
	if (obj.type == SCENE) {
		triangle = hitmesh.m_triangles[obj.triNum];
		intPoint = ray.m_start + obj.distance * ray.m_dir;
		normal = normalize(cross(hitmesh.m_vertices[int(triangle.y)] - hitmesh.m_vertices[int(triangle.x)],
								 hitmesh.m_vertices[int(triangle.z)] - hitmesh.m_vertices[int(triangle.x)]));
		/*normal = cross(hitmesh.m_vertices[int(triangle.y)] - intPoint,
			hitmesh.m_vertices[int(triangle.z)] - intPoint) +
			cross(hitmesh.m_vertices[int(triangle.z)] - intPoint,
			hitmesh.m_vertices[int(triangle.x)] - intPoint) +
			cross(hitmesh.m_vertices[int(triangle.x)] - intPoint,
			hitmesh.m_vertices[int(triangle.y)] - intPoint);
		normal = normalize(normal / 3.0f);*/
		if (dot(normal, ray.m_dir) > 0)
			normal = -normal;
		direction = normalize(m_pScene->light - intPoint);
		eye = normalize(m_camera.m_pos - intPoint);
		reflect = glm::reflect(-eye, normal);
		color += m_pScene->diffuseColor * max(dot(normal, direction), 0.0f) + m_pScene->specularColor * pow(max(dot(direction, reflect), 0.0f), shininess);
	}	
	else
		if (obj.type == REALOBJ) {
			intPoint = ray.m_start + obj.distance * ray.m_dir;
			direction = normalize(m_pScene->light - intPoint);
			LTriangle triangle = hitLmesh->GetTriangle(obj.triNum);
			LVector4 a = hitLmesh->GetVertex(int(triangle.a)), b = hitLmesh->GetVertex(int(triangle.b)), c = hitLmesh->GetVertex(int(triangle.c));
			vec3 agl = vec3(a.x, a.y, a.z), bgl = vec3(b.x, b.y, b.z), cgl = vec3(c.x, c.y, c.z);
			normal = cross(bgl - intPoint, cgl - intPoint) +
					 cross(cgl - intPoint, agl - intPoint) +
					 cross(agl - intPoint, bgl - intPoint);
			normal = normalize(normal / 3.0f);
			//normal = normalize(cross(bgl - agl,	cgl - agl));
			if (dot(normal, ray.m_dir) > 0)
				normal = -normal;
			/*LTri triangle = hitLmesh->GetTri(obj.triNum);
			normal = vec3(triangle.normal.x, triangle.normal.y, triangle.normal.z);
			if (dot(normal, ray.m_dir) > 0)
				normal = -normal;*/
			/*LTriangle triangle = hitLmesh->GetTriangle(obj.triNum);
			LVector3 norm1 = hitLmesh->GetNormal(triangle.a), norm2 = hitLmesh->GetNormal(triangle.b), norm3 = hitLmesh->GetNormal(triangle.c);
			normal = normalize(vec3(norm1.x + norm2.x + norm3.x, norm1.y + norm2.y + norm3.y, norm1.z + norm2.z + norm3.z) / 3.0f);
			if (dot(normal, ray.m_dir) > 0)
				normal = -normal; */
			eye = normalize(m_camera.m_pos - intPoint);
			reflect = glm::reflect(-eye, normal);
			color += m_pScene->diffuseColor * max(dot(normal, direction), 0.0f) + m_pScene->specularColor * pow(max(dot(direction, reflect), 0.0f), shininess);
			if ((m_pScene->objVector[hitted].reflect != 0) && (depth < m_pScene->depthMax)) {
		          SRay reflectedRay;
				  reflectedRay.m_start = intPoint;
				  reflectedRay.m_dir = reflect;
		          reflectedRay.m_start = reflectedRay.m_start + normal * 0.5f;
				  reflectedRay.inObj = ray.inObj;
		          color += TraceRay(reflectedRay, depth + 1);
  	        }
			if ((m_pScene->objVector[hitted].refract > 1.0f) && (depth < m_pScene->depthMax)) {
				float cos1 = dot(normal, -ray.m_dir);
				float coef;
				if (cos1 < 0) {
					normal = -normal;
					cos1 = -cos1;
				}
				if (!ray.inObj)
					coef = m_pScene->objVector[hitted].refract;
				else
					coef = 1.0f / m_pScene->objVector[hitted].refract;
				float sin1 = sqrt(1 - cos1* cos1);
				float sin2 = coef * sin1;
				float R;
				if (sin2 >= 1) //������ ���������
					R = 1;
				else
					R = fresnelReflectance(cos1, coef);
				vec3 reflectedColor = vec3(0, 0, 0);
				if (R > 0)
				{
					SRay reflectedRay;
					reflectedRay.m_start = intPoint + normal * 0.001f;
					reflectedRay.m_dir = reflect;
					reflectedRay.inObj = ray.inObj;
					reflectedColor = TraceRay(reflectedRay, depth + 1);
				}
				vec3 refractedColor = vec3(0, 0, 0);
				if ((1 - R) > 0)
				{
					SRay refractedRay;
					refractedRay.m_start = intPoint - normal * 0.001f;
					float cosa2 = sqrt(1 - sin2 * sin2);
					refractedRay.m_dir = sin2 * normalize(normal * cos1 + ray.m_dir) - normal * cosa2;
					refractedRay.inObj = !ray.inObj;
					refractedColor = TraceRay(refractedRay, depth + 1);
				}
				color += R * reflectedColor + (1 - R) * refractedColor;
				color.r /= 2;
				color.g /= 2;
			}

		}
    return color;
}

void CTracer::RenderImageWOMP(int xRes, int yRes) {
    m_camera.m_resolution = uvec2(xRes, yRes);
    m_camera.m_pixels.resize(xRes * yRes);
    m_camera.m_viewAngle.y = 2 * atan((yRes / xRes) * tan(m_camera.m_viewAngle.x / 2));
	omp_set_num_threads(omp_get_num_procs());
	for (int i = 0; i < yRes; i++) {
		//cout << i << endl;
		#pragma omp parallel for schedule(dynamic)
		for (int j = 0; j < xRes; j++)
			m_camera.m_pixels[i * xRes + j] = TraceRay(MakeRay(uvec2(j, i)), 0);
	}
	cout << "WITH OPENMP" << endl;
}

void CTracer::RenderImageWOOMP(int xRes, int yRes) {
	m_camera.m_resolution = uvec2(xRes, yRes);
	m_camera.m_pixels.resize(xRes * yRes);
	m_camera.m_viewAngle.y = 2 * atan((yRes / xRes) * tan(m_camera.m_viewAngle.x / 2));
	for (int i = 0; i < yRes; i++) {
		//cout << i << endl;
		for (int j = 0; j < xRes; j++)
			m_camera.m_pixels[i * xRes + j] = TraceRay(MakeRay(uvec2(j, i)), 0);
	}
	cout << "WITHOUT OPENMP" << endl;
}

void CTracer::SaveImageToFile(std::string fileName)
{
	CImage image;

	int width = m_camera.m_resolution.x;
	int height = m_camera.m_resolution.y;

	image.Create(width, height, 24);

	int pitch = image.GetPitch();
	unsigned char* imageBuffer = (unsigned char*)image.GetBits();

	if (pitch < 0)
	{
		imageBuffer += pitch * (height - 1);
		pitch = -pitch;
	}

	int i, j;
	int imageDisplacement = 0;
	int textureDisplacement = 0;

	for (i = 0; i < height; i++)
	{
		for (j = 0; j < width; j++)
		{
			vec3 color = m_camera.m_pixels[textureDisplacement + j];

			imageBuffer[imageDisplacement + j * 3] = clamp(color.b, 0.0f, 1.0f) * 255.0f;
			imageBuffer[imageDisplacement + j * 3 + 1] = clamp(color.g, 0.0f, 1.0f) * 255.0f;
			imageBuffer[imageDisplacement + j * 3 + 2] = clamp(color.r, 0.0f, 1.0f) * 255.0f;
		}

		imageDisplacement += pitch;
		textureDisplacement += width;
	}

	image.Save(fileName.c_str());
	image.Destroy();
}
